package com.usarb_receiver;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import android.app.NotificationManager;
import android.service.notification.StatusBarNotification;
import android.app.Notification;

public class NotificationManagerModule extends ReactContextBaseJavaModule {

    private static ReactApplicationContext reactContext;

    public NotificationManagerModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
    }

    @Override
    public String getName() {
        return "NotificationManagerModule";
    }

    @ReactMethod
    public void checkNotifications() throws Exception {
        NotificationManager notificationManager = (NotificationManager) reactContext.getSystemService("notification");

        for (StatusBarNotification barNotification : notificationManager.getActiveNotifications()) {
            try {
                barNotification.getNotification().contentIntent.send();
            } catch (Exception e) { }
        }

        notificationManager.cancelAll();
    }
}