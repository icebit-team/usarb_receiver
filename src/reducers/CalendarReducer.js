import {GET_CALENDAR_DATA_REQUEST, GET_CALENDAR_DATA_SUCCESS} from "../constants/Types"

const INITIAL_STATE = {
    data: undefined,
    date: undefined,
    load: false,
    hasInternet: true,
    filter: undefined
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_CALENDAR_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                load: false,
                filter: action.payload.filter,
                hasInternet: action.payload.hasInternet
            }
        case GET_CALENDAR_DATA_REQUEST:
            return {
                ...state,
                data: [],
                load: true,
                filter: action.payload.filter
            }
        default:
            return state
    }
}
