import {GET_NOTIFICATIONS_DATA_SUCCESS} from "../constants/Types"

const INITIAL_STATE = {
    data: [],
    unReadCount: 0,
    total: 0
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_NOTIFICATIONS_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                unReadCount: action.payload.unReadCount,
                total: action.payload.total,
                last: action.payload.last
            }
        default:
            return state
    }
}
