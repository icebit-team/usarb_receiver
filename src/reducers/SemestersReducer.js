import {GET_SEMESTERS_DATA_SUCCESS} from "../constants/Types"

const INITIAL_STATE = {
    data: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_SEMESTERS_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload
            }
        default:
            return state
    }
}
