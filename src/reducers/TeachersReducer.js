import {GET_TEACHERS_BY_FILTER, GET_TEACHERS_DATA_REFRESH, GET_TEACHERS_DATA_SUCCESS, GET_TEACHERS_NEXT} from "../constants/Types"

const INITIAL_STATE = {
    data: null,
    last: false,
    hasInternet: true,
    filter: null,
    refresh: false
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_TEACHERS_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                last: action.payload.last,
                filter: action.payload.filter,
                hasInternet: action.payload.hasInternet,
                refresh: false
            }
        case GET_TEACHERS_DATA_REFRESH:
            return {
                ...state,
                refresh: true
            }
        case GET_TEACHERS_BY_FILTER:
            return {
                ...state,
                data: action.payload.data,
                filter: action.payload.filter,
                last: action.payload.last
            }
        case GET_TEACHERS_NEXT:
            return {
                ...state,
                data: action.payload.data,
                last: action.payload.last
            }
        default:
            return state
    }
}
