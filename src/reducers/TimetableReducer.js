import {GET_TIMETABLE_DATA_REQUEST, GET_TIMETABLE_DATA_SUCCESS} from "../constants/Types"

const INITIAL_STATE = {
    data: undefined,
    date: undefined,
    load: false,
    hasInternet: true,
    filter: undefined
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_TIMETABLE_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload.data,
                load: false,
                date: action.payload.date,
                filter: action.payload.filter,
                hasInternet: action.payload.hasInternet
            }
        case GET_TIMETABLE_DATA_REQUEST:
            return {
                ...state,
                data: [],
                load: true,
                date: action.payload.date,
                filter: action.payload.filter
            }
        default:
            return state
    }
}
