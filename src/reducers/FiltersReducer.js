import {CHANGE_SELECTED_FILTERS} from "../constants/Types"

const INITIAL_STATE = {
    data: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_SELECTED_FILTERS:
            return {
                ...state,
                data: action.payload
            }
        default:
            return state
    }
}
