import {combineReducers} from 'redux'
import TimetableReducer from "./TimetableReducer"
import GroupsReducer from "./GroupsReducer"
import TeachersReducer from "./TeachersReducer"
import ClassroomsReducer from "./ClassroomsReducer"
import FiltersReducer from "./FiltersReducer"
import NotificationsReducer from "./NotificationsReducer"
import SemestersReducer from "./SemestersReducer"
import CalendarReducer from "./CalendarReducer"

export default combineReducers({
    timetableData: TimetableReducer,

    groups: GroupsReducer,
    teachers: TeachersReducer,
    classrooms: ClassroomsReducer,

    filters: FiltersReducer,

    notifications: NotificationsReducer,

    semesters: SemestersReducer,

    calendar: CalendarReducer
})