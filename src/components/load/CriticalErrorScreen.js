import React, {PureComponent} from "react"
import {connect} from "react-redux"
import {CriticalErrorContainer} from "../shared"

class CriticalErrorComponent extends PureComponent {

    render() {
        return (
            <CriticalErrorContainer/>
        )
    }
}

const CriticalErrorScreen = connect(null, null)(CriticalErrorComponent)
export {CriticalErrorScreen}