import React, {PureComponent} from "react"
import {connect} from "react-redux"
import {configFilter, loadCalendarData, loadClassrooms, loadGroups, loadNotifications, loadSemesters, loadTeachers, loadTimetableData} from "../../actions"
import {BOTTOM_TAB_NAVIGATOR, CRITICAL_ERROR_SCREEN} from "../../constants/Routes"
import {ASYNC_CALL} from "../../utils/AppUtils"
import {ActivityIndicator, Image, StyleSheet, Text, View} from "react-native"
import {WHITE_COLOR, WIDTH} from "../../constants/Global"
import I18n from "react-native-i18n"

class LoadAppComponent extends PureComponent {

    componentDidMount = () => {
        ASYNC_CALL(async () => {
            try {
                await this.props.loadSemesters()

                await this.props.loadClassrooms()
                await this.props.loadTeachers()
                await this.props.loadGroups()

                await this.props.configFilter()

                await this.props.loadNotifications()
                await this.props.loadTimetableData()
                await this.props.loadCalendarData()

                this.props.navigation.navigate(BOTTOM_TAB_NAVIGATOR)
            } catch (e) {
                this.props.navigation.navigate(CRITICAL_ERROR_SCREEN)
            }
        })
    }

    render() {
        return (
            <View style={[styles.cnt, styles.column, {backgroundColor: WHITE_COLOR}]}>
                <View style={[styles.row, {marginBottom: 40, justifyContent: 'center', alignItems: 'center'}]}>
                    <Image style={{width: 117, height: 83}} source={require('../../resources/USBLogo.png')}/>
                    <View style={[styles.column, {justifyContent: 'center', alignItems: 'center', paddingLeft: 20}]}>
                        <Text style={{fontSize: 25, fontWeight: 'bold'}}>USARB</Text>
                        <Text style={{fontSize: 25, fontWeight: 'bold'}}>ReceIVer</Text>
                    </View>
                </View>

                <ActivityIndicator/>
                <Text>{I18n.t('msg.loading')}</Text>

                <View style={[styles.row, {marginTop: 40, justifyContent: 'center', alignItems: 'center'}]}>
                    <Image style={{width: 36, height: 36, marginLeft: 0, marginRight: 0}} source={require('../../resources/React.png')}/>
                    <Image style={{width: 31, height: 31, marginLeft: 3, marginRight: 3}} source={require('../../resources/redux-283024.png')}/>
                    <Image style={{width: 31, height: 31, marginLeft: 3, marginRight: 3}} source={require('../../resources/realm-ibl-infotech.png')}/>
                    <Image style={{width: 31, height: 31, marginLeft: 3, marginRight: 3}} source={require('../../resources/firebase.png')}/>
                    <Image style={{width: 31, height: 31, marginLeft: 3, marginRight: 3}} source={require('../../resources/react-navigation.png')}/>
                </View>
                <View style={[styles.row, {marginTop: 5, justifyContent: 'center', alignItems: 'center'}]}>
                    <Image style={{width: 68, height: 31}} source={require('../../resources/android-app-icon-png-12.jpg')}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    cnt: {
        flex: 1,
        width: WIDTH,
        justifyContent: 'center',
        alignItems: 'center'
    },

    row: {
        flexDirection: 'row'
    },

    column: {
        flexDirection: 'column'
    }
})

const LoadAppScreen = connect(null, {loadTimetableData, loadClassrooms, loadTeachers, loadGroups, configFilter, loadNotifications, loadSemesters, loadCalendarData})(LoadAppComponent)

export {LoadAppScreen}