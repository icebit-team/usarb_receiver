import React, {PureComponent} from "react"
import {Alert, FlatList, RefreshControl, StyleSheet, Text, TouchableOpacity, View} from "react-native"
import {Body, Card, CardItem, Container, Content, Header, Left, ListItem, Right, Title} from 'native-base'
import {CustomIcon, EmptyContainer, ICON_CNT_STYLE, SmallLoadContainer, WhiteIcon} from "../shared"
import moment from "moment"
import {connect} from "react-redux"
import {clearNotifications, loadCalendarData, loadNextNotifications, loadNotifications, loadTimetableData, readNotification} from "../../actions"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import {
    BASE_COLOR,
    DEFAULT_DATE_FORMAT,
    GREEN_COLOR,
    GREY_COLOR,
    ICON_MEDIUM_SIZE,
    ICON_SMALL_SIZE,
    ICON_VERY_SMALL_SIZE,
    LESSON_TIME,
    RED_COLOR,
    WHITE_GREEN_COLOR_2,
    WHITE_RED_COLOR_2,
    WHITE_YALOW_COLOR
} from "../../constants/Global"
import {formatFullDate, formatShortDate} from "../../utils/DateUtils"
import I18n from "react-native-i18n"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import {TIMETABLE_SCREEN} from "../../constants/Routes";
import {hasAccess} from "../../utils/FirebaseUtils";

const NOTIFICATION_ICON = {
    EDIT: 'pencil',
    NEW: 'plus-circle',
    DELETE: 'minus-circle',
}

class NotificationComponent extends PureComponent {

    state = {
        access: false,
        refresh: false
    }

    componentDidMount = async () => this.setState({access: await hasAccess()})

    render() {
        let {notifications, readNotification, loadNotifications, loadNextNotifications, unReadCount, total, isFullData} = this.props,
            {access, refresh} = this.state

        return (
            <Container>
                <Header>
                    <Body>
                        <Title>{I18n.t('menu.notifications')} ({unReadCount}/{total})</Title>
                    </Body>
                    <Right>
                        <CustomIcon name="bolt" color={access ? WHITE_GREEN_COLOR_2 : WHITE_RED_COLOR_2}/>
                        {total > 0 && <WhiteIcon name="trash" styleKey={ICON_CNT_STYLE.AFTER_TEXT} onPress={() => this._onClear()}/>}
                    </Right>
                </Header>

                {total === 0 && <EmptyContainer/>}

                {total > 0 &&
                <Content padder onScroll={(event) => this._checkScroll(event, notifications, loadNextNotifications)}
                         refreshControl={<RefreshControl refreshing={refresh} onRefresh={() => loadNotifications()}/>}>
                    {notifications.map((data, idx) => {
                        let jsonInfo = JSON.parse(data.jsonInfo)

                        return (
                            <TouchableOpacity key={idx} onPress={() => readNotification(data, notifications.length)}>
                                <Card>
                                    <CardItem bordered cardBody>
                                        <View style={[styles.flex, styles.row, styles.padding, !data.read ? styles[data.type] : {}]}>
                                            <View style={styles.row}>
                                                <FontAwesome name={NOTIFICATION_ICON[data.type]} size={ICON_MEDIUM_SIZE} style={[styles.topicIcon, !data.read ? {color: BASE_COLOR} : {}]}/>
                                                <Text numberOfLines={1} style={[styles.bold, !data.read ? {color: BASE_COLOR} : {}]}>{data.topicInfo}</Text>
                                            </View>
                                        </View>
                                    </CardItem>

                                    {data.type === 'EDIT' &&
                                    <CardItem bordered cardBody>
                                        <View style={styles.body}>
                                            <ListItem icon onPress={() => this._onPressItem(data.topicInfo, jsonInfo.date, data)}>
                                                <Left><MaterialCommunityIcons name={`numeric-${jsonInfo.nr}-box-multiple-outline`} size={ICON_MEDIUM_SIZE}/></Left>
                                                <Body><Text numberOfLines={1}>{LESSON_TIME[jsonInfo.nr]}</Text></Body>
                                                <Right><Text style={{marginLeft: 10}}>{formatShortDate(moment(jsonInfo.date, DEFAULT_DATE_FORMAT, true))}</Text></Right>
                                            </ListItem>

                                            <View style={{paddingLeft: 10, paddingRight: 10, paddingBottom: 5, paddingTop: 5}}>
                                                <View style={styles.row}>
                                                    <View style={[styles.column, {flex: 1, borderRightWidth: 1, borderColor: GREY_COLOR, paddingRight: 5, justifyContent: 'center', alignItems: 'center'}]}>
                                                        <FontAwesome name="minus" size={ICON_VERY_SMALL_SIZE} style={[styles.topicIcon, !data.read ? {color: RED_COLOR} : {}]}/>
                                                    </View>
                                                    <View style={[styles.column, {flex: 1, justifyContent: 'center', alignItems: 'center', borderLeftWidth: 1, borderColor: GREY_COLOR, paddingLeft: 5}]}>
                                                        <FontAwesome name="plus" size={ICON_VERY_SMALL_SIZE} style={[styles.topicIcon, !data.read ? {color: GREEN_COLOR} : {}]}/>
                                                    </View>
                                                </View>

                                                <View style={styles.row}>
                                                    <View style={[styles.column, {flex: 1, borderRightWidth: 1, borderColor: GREY_COLOR, paddingRight: 5}]}>
                                                        <Text>{jsonInfo.oldType}</Text>
                                                    </View>
                                                    <View style={[styles.column, {flex: 1, borderLeftWidth: 1, borderColor: GREY_COLOR, paddingLeft: 5}]}>
                                                        <Text>{jsonInfo.newType}</Text>
                                                    </View>
                                                </View>

                                                <View style={styles.row}>
                                                    <View style={[styles.column, {flex: 1, borderRightWidth: 1, borderColor: GREY_COLOR, paddingRight: 5}]}>
                                                        <Text numberOfLines={1}>{jsonInfo.oldDisc}</Text>
                                                    </View>
                                                    <View style={[styles.column, {flex: 1, borderLeftWidth: 1, borderColor: GREY_COLOR, paddingLeft: 5}]}>
                                                        <Text numberOfLines={1}>{jsonInfo.newDisc}</Text>
                                                    </View>
                                                </View>

                                                <View style={styles.row}>
                                                    <View style={[styles.column, {flex: 1, borderRightWidth: 1, borderColor: GREY_COLOR, paddingRight: 5}]}>
                                                        <Text style={styles.right}>{jsonInfo.oldPInfo}</Text>
                                                    </View>
                                                    <View style={[styles.column, {flex: 1, borderLeftWidth: 1, borderColor: GREY_COLOR, paddingLeft: 5}]}>
                                                        <Text style={styles.right}>{jsonInfo.newPInfo}</Text>
                                                    </View>
                                                </View>

                                                <View style={styles.row}>
                                                    <View style={[styles.column, {flex: 1, borderRightWidth: 1, borderColor: GREY_COLOR, paddingRight: 5}]}>
                                                        <Text style={styles.right}>{jsonInfo.oldSInfo}</Text>
                                                    </View>
                                                    <View style={[styles.column, {flex: 1, borderLeftWidth: 1, borderColor: GREY_COLOR, paddingLeft: 5}]}>
                                                        <Text style={styles.right}>{jsonInfo.newSInfo}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    </CardItem>}

                                    {(data.type === 'DELETE' || data.type === 'NEW') &&
                                    <CardItem bordered cardBody style={{marginLeft: 0, paddingLeft: 0}}>
                                        <FlatList data={jsonInfo}
                                                  keyExtractor={(itemData, index) => index.toString()}
                                                  renderItem={({item, index}) => (
                                                      <ListItem icon key={index} last={index === jsonInfo.length - 1} onPress={() => this._onPressItem(data.topicInfo, item.date, data)}>
                                                          <Left><MaterialCommunityIcons name={`numeric-${item.nr}-box-multiple-outline`} size={ICON_MEDIUM_SIZE}/></Left>
                                                          <Body><Text numberOfLines={1}>{item.disc}</Text></Body>
                                                          <Right><Text style={{marginLeft: 10}}>{formatShortDate(moment(item.date, DEFAULT_DATE_FORMAT, true))}</Text></Right>
                                                      </ListItem>
                                                  )}/>
                                    </CardItem>}

                                    <CardItem cardBody>
                                        <View style={[styles.footer, styles.padding, styles.flex]}>
                                            <View style={styles.row}>
                                                <Text style={styles.smallText}>{data.receivedTime}</Text>
                                                <FontAwesome name="clock-o" size={ICON_SMALL_SIZE} style={[styles.smallIcon2, !data.read ? {color: BASE_COLOR} : {}]}/>
                                            </View>
                                            <View style={styles.row}>
                                                <Text style={styles.smallText}>{formatFullDate(moment(data.receivedDate, DEFAULT_DATE_FORMAT, true))}</Text>
                                                <FontAwesome name="calendar-check-o" size={ICON_SMALL_SIZE} style={[styles.smallIcon1, !data.read ? {color: BASE_COLOR} : {}]}/>
                                            </View>
                                        </View>
                                    </CardItem>
                                </Card>
                            </TouchableOpacity>
                        )
                    })}

                    {!isFullData && <SmallLoadContainer/>}
                </Content>}

            </Container>
        )
    }

    _checkScroll = (event, data, onLoadNext) => {
        let scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height
        if (parseInt(scrollHeight) === parseInt(event.nativeEvent.contentSize.height)) {
            onLoadNext(data.length)
        }
    }

    _onPressItem(topic, date, data) {
        this.props.readNotification(data, this.props.notifications.length);

        let filter = {
            data: {
                id: new Date().getTime(),
                name: data.topicInfo
            },
            group: data.group,
            teacher: data.teacher,
            classroom: data.classroom,
        };

        this.props.loadTimetableData(filter, moment(date, DEFAULT_DATE_FORMAT, true))
        this.props.loadCalendarData(filter)
        this.props.navigation.navigate(TIMETABLE_SCREEN)
    }

    _onClear() {
        let {clearNotifications} = this.props

        Alert.alert(
            I18n.t('label.confirmation'), I18n.t('msg.clearNotifications'),
            [{text: I18n.t('label.no'), style: 'cancel'}, {text: I18n.t('label.yes'), onPress: () => clearNotifications()}]
        )
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row'
    },
    bold: {
        fontWeight: 'bold'
    },
    right: {
        textAlign: 'right'
    },
    smallText: {
        fontSize: 12
    },
    flex: {
        flex: 1
    },
    padding: {
        paddingLeft: 10,
        paddingTop: 7,
        paddingRight: 10,
        paddingBottom: 5,
    },
    footer: {
        alignItems: 'flex-end',
        flexDirection: 'column'
    },
    smallIcon1: {
        paddingLeft: 4
    },
    smallIcon2: {
        paddingLeft: 5,
        paddingRight: 1
    },
    topicIcon: {
        paddingRight: 10
    },
    body: {
        flexDirection: 'column',
        flex: 1
    },
    EDIT: {
        backgroundColor: WHITE_YALOW_COLOR
    },
    NEW: {
        backgroundColor: WHITE_GREEN_COLOR_2
    },
    DELETE: {
        backgroundColor: WHITE_RED_COLOR_2
    }
})

const mapStateToProps = state => {
    return {
        notifications: state.notifications.data,
        unReadCount: state.notifications.unReadCount,
        total: state.notifications.total,
        isFullData: state.notifications.last
    }
}

const NotificationScreen = connect(mapStateToProps, {clearNotifications, readNotification, loadTimetableData, loadCalendarData, loadNextNotifications, loadNotifications})(NotificationComponent)

export {NotificationScreen}