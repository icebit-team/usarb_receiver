import React, {PureComponent} from "react"
import {Linking, Text} from "react-native"
import {Body, Container, Content, Header, Icon, Left, ListItem, Right, Separator, Title} from 'native-base'
import {connect} from "react-redux"
import {BASE_COLOR, GREEN_COLOR, ORANGE_COLOR, RED_COLOR, WHITE_VIOLET_COLOR} from "../../constants/Global"
import I18n from "react-native-i18n"

class ContactsComponent extends PureComponent {

    render() {
        return (
            <Container>
                <Header>
                    <Body><Title>{I18n.t('menu.contacts')}</Title></Body>
                    <Right/>
                </Header>

                <Content>
                    <Separator bordered>
                        <Text>{I18n.t('label.university')}</Text>
                    </Separator>
                    <ListItem icon onPress={() => Linking.openURL('mailto:anticamera@usarb.md')}>
                        <Left><Icon type="Entypo" style={{color: BASE_COLOR, fontSize: 22}} name="email"/></Left>
                        <Body><Text>anticamera@usarb.md</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                    <ListItem icon onPress={() => Linking.openURL('tel:+37323152430')}>
                        <Left><Icon type="Entypo" style={{color: GREEN_COLOR, fontSize: 22}} name="phone"/></Left>
                        <Body><Text>+373 231 52430</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                    <ListItem icon onPress={() => Linking.openURL('https://goo.gl/maps/6XG4hoYrQ9RNvtfE6')}>
                        <Left><Icon type="Entypo" style={{color: WHITE_VIOLET_COLOR, fontSize: 22}} name="location"/></Left>
                        <Body><Text>{I18n.t('label.universityAddress')}</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                    <ListItem icon onPress={() => Linking.openURL('https://www.youtube.com/channel/UC1s0JOdKAC_26tswxf7wZCQ')}>
                        <Left><Icon active type="FontAwesome" style={{color: RED_COLOR, fontSize: 24}} name="youtube"/></Left>
                        <Body><Text>{I18n.t('label.youtubeCanal')}</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                    <ListItem icon onPress={() => Linking.openURL('http://usarb.md')}>
                        <Left><Icon type="FontAwesome" style={{color: ORANGE_COLOR, fontSize: 21}} name="chrome"/></Left>
                        <Body><Text>{I18n.t('label.website')}</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                    <ListItem icon last onPress={() => Linking.openURL('https://www.facebook.com/usarb.balti')}>
                        <Left><Icon type="FontAwesome" style={{color: BASE_COLOR, fontSize: 24}} name="facebook-square"/></Left>
                        <Body><Text>{I18n.t('label.facebookPage')}</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>

                    <Separator bordered>
                        <Text>{I18n.t('label.developer')}</Text>
                    </Separator>
                    <ListItem icon onPress={() => Linking.openURL('mailto:vasilov.nicolae@gmail.com')}>
                        <Left><Icon type="Entypo" style={{color: BASE_COLOR, fontSize: 22}} name="email"/></Left>
                        <Body><Text>vasilov.nicolae@gmail.com</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                    <ListItem icon onPress={() => Linking.openURL('https://youtu.be/PMK6srMqddc')}>
                        <Left><Icon type="FontAwesome" style={{color: RED_COLOR, fontSize: 24}} name="youtube"/></Left>
                        <Body><Text>USARB ReceIVer</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                    <ListItem icon last onPress={() => Linking.openURL('https://youtu.be/5ej_Wc21CfY')}>
                        <Left><Icon type="FontAwesome" style={{color: RED_COLOR, fontSize: 24}} name="youtube"/></Left>
                        <Body><Text>Rest Client USARB (rest-client.usarb.md)</Text></Body>
                        <Right><Icon name="arrow-forward"/></Right>
                    </ListItem>
                </Content>
            </Container>
        )
    }
}

const ContactsScreen = connect(null, null)(ContactsComponent)
export {ContactsScreen}