import {ActivityIndicator, StyleSheet, Text, View} from "react-native"
import {BaseIcon, CustomIcon, ICON_CNT_STYLE} from "./Icons"
import {ICON_SIZE, ICON_VERY_SMALL_SIZE, ORANGE_COLOR, RED_COLOR, WIDTH} from "../../constants/Global"
import React from "react"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import I18n from "react-native-i18n"

const EmptyContainer = () => {
    return (
        <View style={[styles.cnt, styles.row]}>
            <BaseIcon styleKey={ICON_CNT_STYLE.BEFORE_TEXT} name="grav"/>
            <Text>{I18n.t('msg.emptyData')}</Text>
        </View>
    )
}


const OfflineContainer = () => {
    return (
        <View style={[styles.smallCnt, styles.row, {backgroundColor: ORANGE_COLOR}]}>
            <FontAwesome name="power-off" size={ICON_VERY_SMALL_SIZE}/>
            <Text numberOfLines={1} style={{marginLeft: 5, fontWeight: 'bold', fontSize: 12}}>{I18n.t('msg.offline')}</Text>
        </View>
    )
}

const WarningContainer = ({msg}) => {
    return (
        <View style={[styles.smallCnt, styles.row, {backgroundColor: ORANGE_COLOR}]}>
            <FontAwesome name="exclamation-circle" size={ICON_VERY_SMALL_SIZE}/>
            <Text numberOfLines={1} style={{marginLeft: 5, fontWeight: 'bold', fontSize: 12}}>{msg}</Text>
        </View>
    )
}

const ErrorContainer = () => {
    return (
        <View style={[styles.cnt, styles.row]}>
            <CustomIcon styleKey={ICON_CNT_STYLE.BEFORE_TEXT} color={RED_COLOR} name="grav"/>
            <Text>{I18n.t('msg.someThinkWentWrong')}</Text>
        </View>
    )
}

const NotAvailableContainer = () => {
    return (
        <View style={[styles.cnt, styles.row]}>
            <FontAwesome5 name="react" style={{paddingRight: 5, color: ORANGE_COLOR}} size={ICON_SIZE}/>
            <Text style={{color: ORANGE_COLOR}}>{I18n.t('msg.inDevelopment')}</Text>
        </View>
    )
}

const CriticalErrorContainer = () => {
    return (
        <View style={[styles.cnt, styles.row]}>
            <CustomIcon styleKey={ICON_CNT_STYLE.BEFORE_TEXT} color={RED_COLOR} name="server"/>
            <Text>{I18n.t('msg.serverError')}</Text>
        </View>
    )
}

const LoadContainer = () => {
    return (
        <View style={[styles.cnt, styles.column]}>
            <ActivityIndicator/>
            <Text>{I18n.t('msg.loading')}</Text>
        </View>
    )
}

const SmallLoadContainer = () => {
    return (
        <View style={{margin: 20}}>
            <ActivityIndicator/>
        </View>
    )
}


const styles = StyleSheet.create({
    cnt: {
        flex: 1,
        width: WIDTH,
        justifyContent: 'center',
        alignItems: 'center'
    },

    smallCnt: {
        paddingLeft: 10,
        height: 18,
        width: WIDTH,
        justifyContent: 'center',
        alignItems: 'center'
    },

    row: {
        flexDirection: 'row'
    },

    column: {
        flexDirection: 'column'
    }
})

export {EmptyContainer, NotAvailableContainer, ErrorContainer, WarningContainer, OfflineContainer, LoadContainer, SmallLoadContainer, CriticalErrorContainer}