import {Body, Container, Content, Header, Input, Item, Left, ListItem, Radio, Right, Title} from "native-base"
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import {BASE_COLOR, ICON_MEDIUM_SIZE, ICON_SIZE, WHITE_COLOR} from "../../constants/Global"
import {RefreshControl, Text} from "react-native"
import {EmptyContainer, OfflineContainer, SmallLoadContainer} from "./CustomContainer"
import React from "react"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import {EntityModel} from "../../constants/Models"
import I18n from "react-native-i18n"

const checkScroll = (event, data, onLoadNext) => {
    let scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height
    if (parseInt(scrollHeight) === parseInt(event.nativeEvent.contentSize.height)) {
        onLoadNext(data.length)
    }
}

const onPressItem = (item, props, filterDetail) => {
    let {loadTimetableData, navigation, loadCalendarData} = props

    loadTimetableData(Object.assign(filterDetail, {data: item}))
    loadCalendarData(Object.assign(filterDetail, {data: item}))
    navigation.goBack()
}

const onSelectItem = (item, props, selected, filterDetail) => {
    let {selectFilter, unselectFilter} = props

    if (selected) {
        unselectFilter(Object.assign(filterDetail, {data: item}))
    } else {
        selectFilter(Object.assign(filterDetail, {data: item}))
    }
}

const GenericFilter = ({props, title, onLoadByFilter, onLoadAll, onLoadNext, selectable, filterDetail}) => {
    let data: EntityModel[] = props.data,
        {navigation, filterValue, isFullData, selectedItems, refresh, hasInternet} = props

    return (
        <Container>
            <Header>
                <Left>
                    <MaterialIcons name="arrow-back" size={ICON_SIZE} style={{color: WHITE_COLOR}} onPress={() => navigation.goBack()}/>
                </Left>
                <Body><Title>{title}</Title></Body>
                <Right/>
            </Header>

            {!hasInternet && <OfflineContainer/>}

            <Item style={{marginRight: 50, marginLeft: 50, marginBottom: 10}}>
                <Input placeholder={I18n.t('label.search')} value={filterValue} onChangeText={text => onLoadByFilter(text)}/>
                <FontAwesome name="search" size={ICON_MEDIUM_SIZE} style={{marginRight: 10}}/>
            </Item>

            {data.length > 0 &&
            <Content onScroll={(event) => checkScroll(event, data, onLoadNext)}
                     refreshControl={<RefreshControl refreshing={refresh} onRefresh={() => onLoadAll()}/>}>
                {data.map((item, idx) => {
                    let selected = selectable ? selectedItems.map(sItem => sItem.data.name).includes(item.name) : false

                    return (
                        <ListItem key={idx}
                                  onPress={() => selectable ? onSelectItem(item, props, selected, filterDetail) : onPressItem(item, props, filterDetail)}
                                  last={idx === data.length - 1}>
                            <Left>
                                <Text style={selectable && selected ? {color: BASE_COLOR} : {}}>
                                    {item.name}
                                </Text>
                            </Left>

                            {selectable &&
                            <Right>
                                <Radio selected={selected} selectedColor={BASE_COLOR}
                                       onPress={() => onSelectItem(item, props, selected, filterDetail)}/>
                            </Right>}
                        </ListItem>
                    )
                })}

                {!isFullData && <SmallLoadContainer/>}
            </Content>}

            {data.length === 0 && <EmptyContainer/>}
        </Container>
    )
}

export {GenericFilter}