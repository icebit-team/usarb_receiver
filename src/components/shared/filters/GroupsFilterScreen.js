import React, {PureComponent} from "react"
import {connect} from "react-redux"
import {loadCalendarData, loadGroups, loadGroupsByFilter, loadNextGroups, loadTimetableData} from "../../../actions"
import {GenericFilter} from ".."
import I18n from "react-native-i18n"

class GroupsFilterComponent extends PureComponent {
    render() {
        let {loadNextGroups, loadGroupsByFilter, loadGroups} = this.props

        return (
            <GenericFilter
                props={this.props}
                title={I18n.t('label.groups')}
                filterDetail={{group: true}}
                onLoadByFilter={loadGroupsByFilter}
                onLoadAll={loadGroups}
                onLoadNext={loadNextGroups}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.groups.data,
        isFullData: state.groups.last,
        filterValue: state.groups.filter,
        hasInternet: state.groups.hasInternet,
        refresh: state.groups.refresh
    }
}

const GroupsFilterScreen = connect(mapStateToProps, {loadTimetableData, loadCalendarData, loadNextGroups, loadGroupsByFilter, loadGroups})(GroupsFilterComponent)

export {GroupsFilterScreen}