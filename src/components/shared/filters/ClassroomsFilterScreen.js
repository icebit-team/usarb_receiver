import React, {PureComponent} from "react"
import {connect} from "react-redux"
import {loadClassrooms, loadClassroomsByFilter, loadCalendarData, loadNextClassrooms, loadTimetableData} from "../../../actions"
import {GenericFilter} from ".."
import I18n from "react-native-i18n"

class ClassroomsFilterComponent extends PureComponent {
    render() {
        let {loadClassroomsByFilter, loadNextClassrooms, loadClassrooms} = this.props

        return (
            <GenericFilter
                props={this.props}
                title={I18n.t('label.classrooms')}
                filterDetail={{classroom: true}}
                onLoadByFilter={loadClassroomsByFilter}
                onLoadAll={loadClassrooms}
                onLoadNext={loadNextClassrooms}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.classrooms.data,
        isFullData: state.classrooms.last,
        filterValue: state.classrooms.filter,
        hasInternet: state.classrooms.hasInternet,
        refresh: state.classrooms.refresh
    }
}

const ClassroomsFilterScreen = connect(mapStateToProps, {loadTimetableData, loadCalendarData, loadClassroomsByFilter, loadNextClassrooms, loadClassrooms})(ClassroomsFilterComponent)

export {ClassroomsFilterScreen}