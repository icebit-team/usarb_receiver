import React, {PureComponent} from "react"
import {connect} from "react-redux"
import {loadCalendarData, loadNextTeachers, loadTeachers, loadTeachersByFilter, loadTimetableData} from "../../../actions"
import {GenericFilter} from ".."
import I18n from "react-native-i18n"

class TeachersFilterComponent extends PureComponent {
    render() {
        let {loadNextTeachers, loadTeachersByFilter, loadTeachers} = this.props

        return (
            <GenericFilter
                props={this.props}
                title={I18n.t('label.teachers')}
                filterDetail={{teacher: true}}
                onLoadByFilter={loadTeachersByFilter}
                onLoadAll={loadTeachers}
                onLoadNext={loadNextTeachers}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.teachers.data,
        isFullData: state.teachers.last,
        filterValue: state.teachers.filter,
        hasInternet: state.teachers.hasInternet,
        refresh: state.teachers.refresh
    }
}

const TeachersFilterScreen = connect(mapStateToProps, {loadTimetableData, loadCalendarData, loadNextTeachers, loadTeachersByFilter, loadTeachers})(TeachersFilterComponent)

export {TeachersFilterScreen}