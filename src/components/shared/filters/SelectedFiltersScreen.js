import React, {PureComponent} from "react"
import {Text} from "react-native"
import {Body, Container, Content, Header, Icon, Left, ListItem, Right, Title} from 'native-base'
import {connect} from "react-redux"
import {loadCalendarData, loadTimetableData} from "../../../actions"
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import {GREEN_COLOR, ICON_SIZE, WHITE_COLOR, WHITE_RED_COLOR, WHITE_VIOLET_COLOR} from "../../../constants/Global"
import {EmptyContainer} from "../CustomContainer"
import I18n from "react-native-i18n"

class SelectedFiltersComponent extends PureComponent {

    render() {
        let {navigation, filters} = this.props

        return (
            <Container>
                <Header>
                    <Left>
                        <MaterialIcons name="arrow-back" size={ICON_SIZE} style={{color: WHITE_COLOR}} onPress={() => navigation.goBack()}/>
                    </Left>
                    <Body><Title>{I18n.t('label.filters2')}</Title></Body>
                    <Right/>
                </Header>

                {filters.length === 0 && <EmptyContainer/>}

                {filters.length > 0 &&
                <Content>
                    {filters.map((item, idx) => (
                        <ListItem icon key={idx} last={idx === filters.length - 1} onPress={() => this._onPressItem(item)}>
                            <Left>
                                {item.classroom && <Icon type="FontAwesome" style={{color: WHITE_RED_COLOR, fontSize: 21}} name="sign-in"/>}
                                {item.teacher && <Icon type="FontAwesome5" style={{color: WHITE_VIOLET_COLOR, fontSize: 18}} name="graduation-cap"/>}
                                {item.group && <Icon type="FontAwesome5" style={{color: GREEN_COLOR, fontSize: 18}} name="users"/>}
                            </Left>
                            <Body><Text>{item.data.name}</Text></Body>
                        </ListItem>
                    ))}
                </Content>}
            </Container>
        )
    }

    _onPressItem = (filter) => {
        let {navigation, loadTimetableData, loadCalendarData} = this.props

        loadTimetableData(filter)
        loadCalendarData(filter)
        navigation.goBack()
    }
}

const mapStateToProps = state => {
    return {
        filters: state.filters.data
    }
}

const SelectedFiltersScreen = connect(mapStateToProps, {loadCalendarData, loadTimetableData})(SelectedFiltersComponent)

export {SelectedFiltersScreen}