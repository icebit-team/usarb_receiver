import React from "react"
import {StyleSheet, View} from "react-native"
import {BASE_COLOR, ICON_SIZE, ICON_MEDIUM_SIZE, WHITE_COLOR} from "../../constants/Global"
import FontAwesome from "react-native-vector-icons/FontAwesome"

const ICON_CNT_STYLE = {
    DEFAULT: 'DEFAULT',
    BEFORE_TEXT: 'BEFORE_TEXT',
    AFTER_TEXT: 'AFTER_TEXT'
}

const getStyle = key => {
    let store = {
        DEFAULT: [styles.default],
        BEFORE_TEXT: [styles.nearText, styles.beforeText],
        AFTER_TEXT: [styles.nearText, styles.afterText]
    }

    return store[key]
}

const WhiteIcon = ({name, onPress, small, styleKey = ICON_CNT_STYLE.DEFAULT}) => {
    return (
        <View style={getStyle(styleKey)}>
            <FontAwesome name={name} size={small ? ICON_MEDIUM_SIZE : ICON_SIZE}
                         style={{color: WHITE_COLOR}} onPress={onPress}/>
        </View>
    )
}

const BaseIcon = ({name, onPress, small, styleKey = ICON_CNT_STYLE.DEFAULT}) => {
    return (
        <View style={getStyle(styleKey)}>
            <FontAwesome name={name} size={small ? ICON_MEDIUM_SIZE : ICON_SIZE}
                         style={{color: BASE_COLOR}} onPress={onPress}/>
        </View>
    )
}

const DefaultIcon = ({name, onPress, small, styleKey = ICON_CNT_STYLE.DEFAULT}) => {
    return (
        <View style={getStyle(styleKey)}>
            <FontAwesome name={name} size={small ? ICON_MEDIUM_SIZE : ICON_SIZE} onPress={onPress}/>
        </View>
    )
}

const CustomIcon = ({name, color, onPress, small, styleKey = ICON_CNT_STYLE.DEFAULT}) => {
    return (
        <View style={getStyle(styleKey)}>
            <FontAwesome name={name} size={small ? ICON_MEDIUM_SIZE : ICON_SIZE}
                         style={color ? {color: color} : {}} onPress={onPress}/>
        </View>
    )
}

export {WhiteIcon, BaseIcon, DefaultIcon, CustomIcon, ICON_CNT_STYLE}


const styles = StyleSheet.create({
    default: {},
    nearText: {
        width: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    beforeText: {
        paddingRight: 5
    },
    afterText: {
        paddingLeft: 5
    }
})