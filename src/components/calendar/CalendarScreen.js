import React, {PureComponent} from "react"
import {StyleSheet, Text, TouchableOpacity, View} from "react-native"
import {Button, Card, Container, Content, Fab, Header, Icon, Left, Right, Title} from 'native-base'
import {connect} from "react-redux"
import moment from 'moment'
import {
    BASE_COLOR,
    CALENDAR_WEEK_DAYS,
    DEFAULT_DATE_FORMAT,
    GREEN_COLOR,
    GREY_COLOR,
    ORANGE_COLOR,
    WHITE_BASE_COLOR,
    WHITE_COLOR,
    WHITE_GREEN_COLOR,
    WHITE_GREY_COLOR,
    WHITE_RED_COLOR,
    WHITE_VIOLET_COLOR
} from "../../constants/Global"
import {EmptyContainer, LoadContainer, OfflineContainer} from "../shared"
import {CALENDAR_CLASSROOMS_FILTER_SCREEN, CALENDAR_FULL_SCREEN, CALENDAR_GROUPS_FILTER_SCREEN, CALENDAR_SELECTED_FILTERS_SCREEN, CALENDAR_TEACHERS_FILTER_SCREEN} from "../../constants/Routes"
import {buildCardHeader, getCurrentDate, getCurrentStartMonth, haveHoursByMonth, isCalendarCardDay} from "./CalendarUtils"
import I18n from "react-native-i18n"

class CalendarComponent extends PureComponent {

    state = {
        active: false
    }

    months = [7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6]
    days = []

    firstDate
    endDate

    componentWillMount() {
        let firstSemester = this.props.semesters.find(item => item.semesterNumber === 1)
        let secondSemester = this.props.semesters.find(item => item.semesterNumber === 2)

        this.firstDate = firstSemester ? moment(firstSemester.date, DEFAULT_DATE_FORMAT, true) : moment()
        this.endDate = secondSemester ? moment(secondSemester.date, DEFAULT_DATE_FORMAT, true) : moment()

        for (let i = 1; i <= 42; i++) this.days.push(i)
    }

    render() {
        let {firstDate, endDate, days} = this,
            {data, filter, load, hasInternet} = this.props,
            {navigate} = this.props.navigation

        let months = this.months.filter(monthIdx => haveHoursByMonth(monthIdx, data))

        return (
            <Container>
                <Header>
                    <Left><Title>{I18n.t('menu.calendar')}</Title></Left>
                    <Right>
                        <View>
                            <Text style={{color: WHITE_COLOR, textAlign: 'right'}} numberOfLines={1}>
                                {firstDate.year() === endDate.year() ? 'N/A' : firstDate.format('YYYY')} - {firstDate.year() === endDate.year() ? 'N/A' : endDate.format('YYYY')}
                            </Text>
                            <Text style={{color: WHITE_COLOR, textAlign: 'right'}} numberOfLines={1}>
                                {filter ? filter.data.name : 'N/A'}
                            </Text>
                        </View>
                    </Right>
                </Header>

                {!hasInternet && <OfflineContainer/>}

                {load && <LoadContainer/>}

                {!load && data.length === 0 && <EmptyContainer/>}

                {!load && data.length > 0 &&
                <Content padder style={{paddingBottom: 50}}>
                    {data.length > 0 && <View style={styles.flexCnt}>

                        {months.map((monthIdx, idx) => {
                            let startMonth = getCurrentStartMonth(monthIdx, firstDate, endDate)

                            return (
                                <TouchableOpacity style={[styles.cardWrapper, idx === months.length - 1 ? {marginBottom: 100} : {}]} key={idx} onPress={() => navigate(CALENDAR_FULL_SCREEN, {monthIdx: monthIdx})}>
                                    <Card style={styles.card}>
                                        <Text style={styles.cardHeader}>{buildCardHeader(monthIdx, firstDate, endDate)}</Text>

                                        <View style={styles.flexCntCard}>
                                            {CALENDAR_WEEK_DAYS.map(dayName => {
                                                return (<View key={dayName} style={[styles.cardItem, {borderWidth: 0}]}>
                                                    <Text style={[{fontSize: 10}, {fontWeight: 'bold'}]}>{I18n.t(['shortDays', dayName])}</Text>
                                                </View>)
                                            })}
                                        </View>

                                        <View style={styles.flexCntCard}>
                                            {days.map(dayIdx => {
                                                let currentDate = getCurrentDate(startMonth, dayIdx)
                                                let items = data.find(item => item.date === currentDate.format(DEFAULT_DATE_FORMAT))
                                                let isCardDay = isCalendarCardDay(dayIdx, startMonth)

                                                return (
                                                    <View key={dayIdx} style={[
                                                        styles.cardItem,
                                                        !isCardDay ? {backgroundColor: WHITE_GREY_COLOR} : {},
                                                        isCardDay && items && currentDate.isSame(moment(), 'days') ? {backgroundColor: GREEN_COLOR} : {},
                                                        isCardDay && !items && currentDate.isSame(moment(), 'days') ? {backgroundColor: BASE_COLOR} : {},
                                                        isCardDay && items && currentDate.isAfter(moment(), 'days') ? {backgroundColor: WHITE_GREEN_COLOR} : {},
                                                        isCardDay && items && currentDate.isBefore(moment(), 'days') ? {backgroundColor: WHITE_BASE_COLOR} : {}
                                                    ]}>
                                                        {isCardDay && items &&
                                                        <Text style={[
                                                            {fontSize: 11},
                                                            currentDate.isSame(moment(), 'days') ? {color: WHITE_COLOR, fontWeight: 'bold'} : {}
                                                        ]}>
                                                            {items.hours}
                                                        </Text>}
                                                    </View>
                                                )
                                            })}
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                            )
                        })}
                    </View>}
                </Content>}

                <Fab
                    active={this.state.active} direction="up" style={{backgroundColor: BASE_COLOR}}
                    position="bottomRight"
                    onPress={() => this.setState({active: !this.state.active})}>
                    <Icon type="FontAwesome" name="filter"/>
                    <Button style={{backgroundColor: ORANGE_COLOR}} onPress={() => navigate(CALENDAR_SELECTED_FILTERS_SCREEN)}>
                        <Icon type="FontAwesome" name="star"/>
                    </Button>
                    <Button style={{backgroundColor: GREEN_COLOR}} onPress={() => navigate(CALENDAR_GROUPS_FILTER_SCREEN)}>
                        <Icon type="FontAwesome5" name="users"/>
                    </Button>
                    <Button style={{backgroundColor: WHITE_VIOLET_COLOR}} onPress={() => navigate(CALENDAR_TEACHERS_FILTER_SCREEN)}>
                        <Icon type="FontAwesome5" name="graduation-cap"/>
                    </Button>
                    <Button style={{backgroundColor: WHITE_RED_COLOR}} onPress={() => navigate(CALENDAR_CLASSROOMS_FILTER_SCREEN)}>
                        <Icon type="FontAwesome" name="sign-in"/>
                    </Button>
                </Fab>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    flexCnt: {
        flexDirection: 'row',
        flexWrap: "wrap",
        alignItems: "flex-start",
        justifyContent: "space-between"
    },
    flexCntCard: {
        flexDirection: 'row',
        flexWrap: "wrap",
        alignItems: "flex-start"
    },
    cardWrapper: {
        width: '49%'
    },
    card: {
        padding: 5
    },
    cardHeader: {
        fontWeight: 'bold',
        fontSize: 12,
        marginLeft: 5,
        marginBottom: 5
    },
    cardItem: {
        width: '14.2857%',
        height: 17,
        borderWidth: 0.5,
        borderColor: GREY_COLOR,
        alignItems: "center",
        justifyContent: "center"
    }
})

const mapStateToProps = state => {
    return {
        semesters: state.semesters.data,
        data: state.calendar.data,
        filter: state.calendar.filter,
        load: state.calendar.load,
        hasInternet: state.calendar.hasInternet
    }
}

const CalendarScreen = connect(mapStateToProps, null)(CalendarComponent)
export {CalendarScreen}