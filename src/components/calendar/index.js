import {createStackNavigator} from "react-navigation"
import {CALENDAR_CLASSROOMS_FILTER_SCREEN, CALENDAR_FULL_SCREEN, CALENDAR_GROUPS_FILTER_SCREEN, CALENDAR_SCREEN, CALENDAR_SELECTED_FILTERS_SCREEN, CALENDAR_TEACHERS_FILTER_SCREEN} from "../../constants/Routes"
import {CalendarScreen} from "./CalendarScreen"
import {CalendarFullScreen} from "./CalendarFullScreen"
import {ClassroomsFilterScreen, GroupsFilterScreen, SelectedFiltersScreen, TeachersFilterScreen} from "../shared"

const CalendarStackNavigator = createStackNavigator({
    [CALENDAR_SCREEN]: {
        screen: CalendarScreen
    },
    [CALENDAR_FULL_SCREEN]: {
        screen: CalendarFullScreen
    },
    [CALENDAR_GROUPS_FILTER_SCREEN]: {
        screen: GroupsFilterScreen
    },
    [CALENDAR_CLASSROOMS_FILTER_SCREEN]: {
        screen: ClassroomsFilterScreen
    },
    [CALENDAR_TEACHERS_FILTER_SCREEN]: {
        screen: TeachersFilterScreen
    },
    [CALENDAR_SELECTED_FILTERS_SCREEN]: {
        screen: SelectedFiltersScreen
    }
}, {
    headerMode: 'none'
})

export {CalendarStackNavigator}