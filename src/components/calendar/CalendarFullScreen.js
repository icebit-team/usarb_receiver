import React, {PureComponent} from "react"
import {StyleSheet, Text, TouchableOpacity, View} from "react-native"
import {Body, Card, Container, Content, Header, Left, Right, Title} from 'native-base'
import {connect} from "react-redux"
import {BASE_COLOR, CALENDAR_WEEK_DAYS, DEFAULT_DATE_FORMAT, GREEN_COLOR, GREY_COLOR, ICON_SIZE, WHITE_BASE_COLOR, WHITE_COLOR, WHITE_GREEN_COLOR, WHITE_GREY_COLOR} from "../../constants/Global"
import MaterialIcons from "react-native-vector-icons/MaterialIcons"
import {buildCardHeader, getCurrentDate, getCurrentStartMonth, isCalendarCardDay} from "./CalendarUtils"
import moment from 'moment'
import {loadTimetableData} from "../../actions"
import {TIMETABLE_SCREEN} from "../../constants/Routes"
import I18n from "react-native-i18n"

class CalendarFullComponent extends PureComponent {

    days = []

    firstDate
    endDate

    componentWillMount() {
        let firstSemester = this.props.semesters.find(item => item.semesterNumber === 1)
        let secondSemester = this.props.semesters.find(item => item.semesterNumber === 2)

        this.firstDate = firstSemester ? moment(firstSemester.date, DEFAULT_DATE_FORMAT, true) : moment()
        this.endDate = secondSemester ? moment(secondSemester.date, DEFAULT_DATE_FORMAT, true) : moment()

        for (let i = 1; i <= 42; i++) this.days.push(i)
    }

    render() {
        let {firstDate, endDate, days} = this,
            {data, navigation, loadTimetableData} = this.props

        let monthIdx = navigation.state.params.monthIdx
        let startMonth = getCurrentStartMonth(monthIdx, firstDate, endDate)

        return (
            <Container>
                <Header>
                    <Left>
                        <MaterialIcons name="arrow-back" size={ICON_SIZE} style={{color: WHITE_COLOR}} onPress={() => navigation.goBack()}/>
                    </Left>
                    <Body>
                    <Title>
                        {buildCardHeader(monthIdx, firstDate, endDate)}
                    </Title>
                    </Body>
                    <Right/>
                </Header>

                <Content padder>
                    <Card style={styles.card}>
                        <View style={styles.flexCntCard}>
                            {CALENDAR_WEEK_DAYS.map(dayName => {
                                return (<View key={dayName} style={[styles.cardItem, {borderWidth: 0}]}>
                                    <Text style={[{fontSize: 14, height: 25}, {fontWeight: 'bold'}]}>{I18n.t(['shortDays', dayName])}</Text>
                                </View>)
                            })}
                        </View>

                        <View style={styles.flexCntCard}>
                            {days.map(dayIdx => {
                                let currentDate = getCurrentDate(startMonth, dayIdx)
                                let items = data.find(item => item.date === currentDate.format(DEFAULT_DATE_FORMAT))
                                let isCardDay = isCalendarCardDay(dayIdx, startMonth)

                                return (
                                    <TouchableOpacity key={dayIdx} onPress={() => {
                                        loadTimetableData(null, currentDate)
                                        navigation.navigate(TIMETABLE_SCREEN)
                                    }} style={[
                                        styles.cardItem, {height: 55},
                                        !isCardDay ? {backgroundColor: WHITE_GREY_COLOR} : {},
                                        isCardDay && items && currentDate.isSame(moment(), 'days') ? {backgroundColor: GREEN_COLOR} : {},
                                        isCardDay && !items && currentDate.isSame(moment(), 'days') ? {backgroundColor: BASE_COLOR} : {},
                                        isCardDay && items && currentDate.isAfter(moment(), 'days') ? {backgroundColor: WHITE_GREEN_COLOR} : {},
                                        isCardDay && items && currentDate.isBefore(moment(), 'days') ? {backgroundColor: WHITE_BASE_COLOR} : {}
                                    ]}>
                                        <View style={styles.dateWrapper}>
                                            <Text style={[
                                                styles.date,
                                                isCardDay && currentDate.isSame(moment(), 'days') ? {color: WHITE_COLOR, fontWeight: 'bold'} : {}
                                            ]}>{currentDate.format('D')}</Text>
                                        </View>

                                        <View style={styles.detailsWrapper}>
                                            <Text style={[
                                                styles.details,
                                                currentDate.isSame(moment(), 'days') ? {color: WHITE_COLOR, fontWeight: 'bold'} : {}
                                            ]}>
                                                {isCardDay && items && items.hours}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })}
                        </View>
                    </Card>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    flexCnt: {
        flexDirection: 'row',
        flexWrap: "wrap",
        alignItems: "flex-start",
        justifyContent: "space-between"
    },
    flexCntCard: {
        flexDirection: 'row',
        flexWrap: "wrap",
        alignItems: "flex-start"
    },
    card: {
        padding: 5
    },
    dateWrapper: {
        width: '100%',
        alignItems: "flex-end"
    },
    date: {
        fontSize: 12
    },
    detailsWrapper: {
        width: '100%',
        alignItems: "flex-start"
    },
    details: {
        fontSize: 20,
        paddingTop: 5
    },
    cardItem: {
        width: '14.2857%',
        paddingLeft: 5,
        paddingRight: 5,
        borderWidth: 0.5,
        borderColor: GREY_COLOR,
        alignItems: "center",
        justifyContent: "center"
    }
})


const mapStateToProps = state => {
    return {
        semesters: state.semesters.data,
        data: state.calendar.data
    }
}

const CalendarFullScreen = connect(mapStateToProps, {loadTimetableData})(CalendarFullComponent)

export {CalendarFullScreen}