import moment from 'moment'
import I18n from "react-native-i18n"

export const haveHoursByMonth = (monthIdx, data) => data
    .some(item => item.date.match(new RegExp('\\d{2}\/' + (monthIdx < 9 ? '0' : '') + (monthIdx + 1) + '\/\\d{4}')))

export const buildCardHeader = (monthIdx, firstDate, endDate) => `${getMonthInfo(monthIdx)} ${getYearInfo(monthIdx, firstDate, endDate)}`

const getMonthInfo = (monthIdx) => I18n.t(['months', moment().month(monthIdx).format('MMMM').toLowerCase()])

const getYearInfo = (monthIdx, firstDate, endDate) => moment((moment().month(monthIdx).month() >= 7 ? firstDate : endDate)).format('YYYY')

export const getCurrentStartMonth = (monthIdx, firstDate, endDate) => moment((moment().month(monthIdx).month() >= 7 ? firstDate : endDate)).month(monthIdx).startOf('month')

export const getCurrentDate = (startMonth, dayIdx) => moment(startMonth).add(dayIdx - moment(startMonth).isoWeekday(), 'days')

export const isCalendarCardDay = (dayIdx, startMonth) => {
    let endMonth = moment(startMonth).endOf('month')
    return (dayIdx >= startMonth.isoWeekday() || dayIdx > 7) && endMonth.date() > dayIdx - startMonth.isoWeekday()
}