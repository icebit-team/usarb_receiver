import React, {PureComponent} from "react"
import {connect} from "react-redux"
import {loadNextTeachers, loadTeachers, loadTeachersByFilter, selectFilter, unselectFilter} from "../../actions"
import {GenericFilter} from "../shared"
import I18n from "react-native-i18n"

class SetTeachersComponent extends PureComponent {
    render() {
        let {loadNextTeachers, loadTeachersByFilter, loadTeachers} = this.props

        return (
            <GenericFilter
                props={this.props}
                title={I18n.t('label.teachers')}
                selectable={true}
                filterDetail={{teacher: true}}
                onLoadByFilter={loadTeachersByFilter}
                onLoadAll={loadTeachers}
                onLoadNext={loadNextTeachers}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.teachers.data,
        isFullData: state.teachers.last,
        filterValue: state.teachers.filter,
        hasInternet: state.teachers.hasInternet,
        selectedItems: state.filters.data.filter(item => item.teacher),
        refresh: state.teachers.refresh
    }
}

const SetTeachersScreen = connect(mapStateToProps, {unselectFilter, selectFilter, loadNextTeachers, loadTeachersByFilter, loadTeachers})(SetTeachersComponent)

export {SetTeachersScreen}