import {createStackNavigator} from "react-navigation"
import {SET_CLASSROOMS_SCREEN, SET_GROUPS_SCREEN, SET_TEACHERS_SCREEN, SETTINGS_SCREEN} from "../../constants/Routes"
import {SettingsScreen} from "./SettingsScreen"
import {SetTeachersScreen} from "./SetTeachersScreen"
import {SetClassroomsScreen} from "./SetClassroomsScreen"
import {SetGroupsScreen} from "./SetGroupsScreen"

const SettingsStackNavigator = createStackNavigator({
    [SETTINGS_SCREEN]: {
        screen: SettingsScreen
    },
    [SET_TEACHERS_SCREEN]: {
        screen: SetTeachersScreen
    },
    [SET_CLASSROOMS_SCREEN]: {
        screen: SetClassroomsScreen
    },
    [SET_GROUPS_SCREEN]: {
        screen: SetGroupsScreen
    }
}, {
    headerMode: 'none'
})

export {SettingsStackNavigator}