import React, {PureComponent} from "react"
import {connect} from "react-redux"
import {loadGroups, loadGroupsByFilter, loadNextGroups, selectFilter, unselectFilter} from "../../actions"
import {GenericFilter} from "../shared"
import I18n from "react-native-i18n"

class SetGroupsComponent extends PureComponent {
    render() {
        let {loadNextGroups, loadGroupsByFilter, loadGroups} = this.props

        return (
            <GenericFilter
                props={this.props}
                title={I18n.t('label.groups')}
                selectable={true}
                filterDetail={{group: true}}
                onLoadByFilter={loadGroupsByFilter}
                onLoadAll={loadGroups}
                onLoadNext={loadNextGroups}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.groups.data,
        isFullData: state.groups.last,
        filterValue: state.groups.filter,
        hasInternet: state.groups.hasInternet,
        selectedItems: state.filters.data.filter(item => item.group),
        refresh: state.groups.refresh
    }
}

const SetGroupsScreen = connect(mapStateToProps, {unselectFilter, selectFilter, loadGroupsByFilter, loadNextGroups, loadGroups})(SetGroupsComponent)

export {SetGroupsScreen}