import React, {PureComponent} from "react"
import {connect} from "react-redux"
import {loadClassrooms, loadClassroomsByFilter, loadNextClassrooms, selectFilter, unselectFilter} from "../../actions"
import {GenericFilter} from "../shared"
import I18n from "react-native-i18n"

class SetClassroomsComponent extends PureComponent {
    render() {
        let {loadClassroomsByFilter, loadNextClassrooms, loadClassrooms} = this.props

        return (
            <GenericFilter
                props={this.props}
                title={I18n.t('label.classrooms')}
                selectable={true}
                filterDetail={{classroom: true}}
                onLoadByFilter={loadClassroomsByFilter}
                onLoadAll={loadClassrooms}
                onLoadNext={loadNextClassrooms}
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.classrooms.data,
        isFullData: state.classrooms.last,
        filterValue: state.classrooms.filter,
        hasInternet: state.classrooms.hasInternet,
        selectedItems: state.filters.data.filter(item => item.classroom),
        refresh: state.classrooms.refresh
    }
}

const SetClassroomsScreen = connect(mapStateToProps, {unselectFilter, selectFilter, loadNextClassrooms, loadClassroomsByFilter, loadClassrooms})(SetClassroomsComponent)

export {SetClassroomsScreen}