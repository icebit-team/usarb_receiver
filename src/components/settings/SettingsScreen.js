import React, {PureComponent} from "react"
import {Alert, AsyncStorage, Image, Text} from "react-native"
import {Body, Button, Container, Content, Fab, Header, Icon, Left, ListItem, Radio, Right, Separator, Title} from 'native-base'
import {connect} from "react-redux"
import {SET_CLASSROOMS_SCREEN, SET_GROUPS_SCREEN, SET_TEACHERS_SCREEN} from "../../constants/Routes"
import {WhiteIcon} from "../shared"
import {clearFilterStorage, subscribeToFilter} from "../../actions"
import {BASE_COLOR, EMPTY_FN, EN_LANG, GREEN_COLOR, ORANGE_COLOR, RO_LANG, RU_LANG, SELECTED_LANG, WHITE_RED_COLOR, WHITE_VIOLET_COLOR} from "../../constants/Global"
import RNRestart from 'react-native-restart'
import I18n from "react-native-i18n"

class SettingsComponent extends PureComponent {
    state = {
        active: false
    }

    render() {
        let {navigate} = this.props.navigation,
            {active} = this.state,
            {filters} = this.props,
            langKey = I18n.currentLocale()

        return (
            <Container>
                <Header>
                    <Body><Title>{I18n.t('menu.settings')}</Title></Body>
                    <Right>
                        {filters.length > 0 && <WhiteIcon name="trash" onPress={() => this._onClear()}/>}
                    </Right>
                </Header>

                <Content>
                    <Separator bordered>
                        <Text>{I18n.t('label.filters')}</Text>
                    </Separator>
                    {filters.map((item, idx) => (
                        <ListItem icon key={idx} last={idx === filters.length - 1} onPress={() => this._onPressItem(item)}>
                            <Left>
                                {item.classroom && <Icon type="FontAwesome" style={{color: WHITE_RED_COLOR, fontSize: 21}} name="sign-in"/>}
                                {item.teacher && <Icon type="FontAwesome5" style={{color: WHITE_VIOLET_COLOR, fontSize: 18}} name="graduation-cap"/>}
                                {item.group && <Icon type="FontAwesome5" style={{color: GREEN_COLOR, fontSize: 18}} name="users"/>}
                            </Left>
                            <Body><Text style={item.subscribe ? {color: BASE_COLOR} : {}}>{item.data.name}</Text></Body>
                            <Right>
                                <Icon type="FontAwesome" style={[item.subscribe ? {color: BASE_COLOR} : {}, {fontSize: 16, marginRight: 2}]} name="envelope"
                                      onPress={() => this._onPressItem(item)}/>
                            </Right>
                        </ListItem>
                    ))}

                    <Separator bordered>
                        <Text>{I18n.t('label.language')}</Text>
                    </Separator>
                    <ListItem icon onPress={() => langKey !== EN_LANG ? this._onPressLang(EN_LANG) : EMPTY_FN}>
                        <Left><Image style={{width: 22, height: 22}} source={require('../../resources/united-states-of-america.png')}/></Left>
                        <Body><Text style={{color: BASE_COLOR}}>{I18n.t('label.english')}</Text></Body>
                        <Right><Radio selected={langKey === EN_LANG} selectedColor={BASE_COLOR}
                                      onPress={() => langKey !== EN_LANG ? this._onPressLang(EN_LANG) : EMPTY_FN}/></Right>
                    </ListItem>
                    <ListItem icon onPress={() => langKey !== RO_LANG ? this._onPressLang(RO_LANG) : EMPTY_FN}>
                        <Left><Image style={{width: 22, height: 22}} source={require('../../resources/romania.png')}/></Left>
                        <Body><Text style={{color: BASE_COLOR}}>{I18n.t('label.romanian')}</Text></Body>
                        <Right><Radio selected={langKey === RO_LANG} selectedColor={BASE_COLOR}
                                      onPress={() => langKey !== RO_LANG ? this._onPressLang(RO_LANG) : EMPTY_FN}/></Right>
                    </ListItem>
                    <ListItem icon last onPress={() => langKey !== RU_LANG ? this._onPressLang(RU_LANG) : EMPTY_FN}>
                        <Left><Image style={{width: 22, height: 22}} source={require('../../resources/russia.png')}/></Left>
                        <Body><Text style={{color: BASE_COLOR}}>{I18n.t('label.russian')}</Text></Body>
                        <Right><Radio selected={langKey === RU_LANG} selectedColor={BASE_COLOR}
                                      onPress={() => langKey !== RU_LANG ? this._onPressLang(RU_LANG) : EMPTY_FN}/></Right>
                    </ListItem>
                </Content>

                <Fab
                    active={active} direction="up" style={{backgroundColor: ORANGE_COLOR}}
                    position="bottomRight"
                    onPress={() => this.setState({active: !active})}>
                    <Icon type="FontAwesome" name="star"/>
                    <Button style={{backgroundColor: GREEN_COLOR}} onPress={() => navigate(SET_GROUPS_SCREEN)}>
                        <Icon type="FontAwesome5" name="users"/>
                    </Button>
                    <Button style={{backgroundColor: WHITE_VIOLET_COLOR}} onPress={() => navigate(SET_TEACHERS_SCREEN)}>
                        <Icon type="FontAwesome5" name="graduation-cap"/>
                    </Button>
                    <Button style={{backgroundColor: WHITE_RED_COLOR}} onPress={() => navigate(SET_CLASSROOMS_SCREEN)}>
                        <Icon type="FontAwesome" name="sign-in"/>
                    </Button>
                </Fab>
            </Container>
        )
    }

    _onPressItem = (item) => {
        let {subscribeToFilter} = this.props

        Alert.alert(
            I18n.t('label.confirmation'), I18n.t('msg.changeSubscribe'),
            [{text: I18n.t('label.no'), style: 'cancel'}, {text: I18n.t('label.yes'), onPress: () => subscribeToFilter(item)}]
        )
    }

    _onPressLang = (key) => {
        Alert.alert(
            I18n.t('label.confirmation'), I18n.t('msg.changeLanguage'),
            [{text: I18n.t('label.no'), style: 'cancel'}, {
                text: I18n.t('label.yes'), onPress: () => {
                    AsyncStorage.setItem(SELECTED_LANG, key)
                    RNRestart.Restart()
                }
            }]
        )
    }

    _onClear = () => {
        let {clearFilterStorage} = this.props

        Alert.alert(
            I18n.t('label.confirmation'), I18n.t('msg.clearSettings'),
            [{text: I18n.t('label.no'), style: 'cancel'}, {text: I18n.t('label.yes'), onPress: () => clearFilterStorage()}]
        )
    }
}

const mapStateToProps = state => {
    return {
        filters: state.filters.data
    }
}

const SettingsScreen = connect(mapStateToProps, {clearFilterStorage, subscribeToFilter})(SettingsComponent)

export {SettingsScreen}