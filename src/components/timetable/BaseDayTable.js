import {Card, CardItem, Content} from 'native-base'
import React from "react"
import {BASE_COLOR, ICON_MEDIUM_SIZE, LESSON_TIME, WIDTH} from "../../constants/Global"
import {EmptyContainer} from "../shared"
import {StyleSheet, Text, View} from "react-native"
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"

export default ({data}) => {

    if (data.length === 0) {
        return (
            <EmptyContainer/>
        )
    } else {
        return (
            <Content padder style={{width: WIDTH}}>
                {data.map((item, idx) => (
                    <Card key={idx} style={idx === data.length - 1 ? {marginBottom: 100} : {}}>
                        <CardItem bordered cardBody>
                            <View style={[styles.header, styles.row]}>
                                <View style={styles.row}>
                                    <MaterialCommunityIcons name={`numeric-${item.lessonNumber}-box-multiple-outline`} size={ICON_MEDIUM_SIZE} style={styles.icon}/>
                                    <Text numberOfLines={1} style={styles.bold}>{LESSON_TIME[item.lessonNumber]}</Text>
                                </View>
                            </View>
                        </CardItem>
                        {item.items.map((lesson, idx) => (
                            <CardItem key={idx} bordered>
                                <View style={styles.body}>
                                    <Text>{lesson.lessonType}</Text>
                                    <Text style={styles.bold}>{lesson.discipline}</Text>
                                    <Text style={styles.right}>{lesson.secondaryInfo}</Text>
                                    <Text style={styles.right}>{lesson.primaryInfo}</Text>
                                </View>
                            </CardItem>
                        ))}
                    </Card>
                ))}
            </Content>
        )
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row'
    },
    bold: {
        fontWeight: 'bold'
    },
    right: {
        textAlign: 'right'
    },
    smallText: {
        fontSize: 12
    },
    header: {
        flex: 1,
        paddingLeft: 10,
        paddingTop: 7,
        paddingRight: 10,
        paddingBottom: 5
    },
    smallIcon: {
        color: BASE_COLOR,
        paddingLeft: 5
    },
    icon: {
        color: BASE_COLOR,
        paddingRight: 10
    },
    body: {
        flexDirection: 'column',
        flex: 1
    }
})