import {createStackNavigator} from "react-navigation"
import {
    TIMETABLE_CLASSROOMS_FILTER_SCREEN,
    TIMETABLE_GROUPS_FILTER_SCREEN,
    TIMETABLE_SELECTED_FILTERS_SCREEN,
    TIMETABLE_TEACHERS_FILTER_SCREEN,
    TIMETABLE_SCREEN
} from "../../constants/Routes"
import {TimetableScreen} from "./TimetableScreen"
import {ClassroomsFilterScreen, GroupsFilterScreen, SelectedFiltersScreen, TeachersFilterScreen} from "../shared"

const TimetableStackNavigator = createStackNavigator({
    [TIMETABLE_SCREEN]: {
        screen: TimetableScreen
    },
    [TIMETABLE_GROUPS_FILTER_SCREEN]: {
        screen: GroupsFilterScreen
    },
    [TIMETABLE_CLASSROOMS_FILTER_SCREEN]: {
        screen: ClassroomsFilterScreen
    },
    [TIMETABLE_TEACHERS_FILTER_SCREEN]: {
        screen: TeachersFilterScreen
    },
    [TIMETABLE_SELECTED_FILTERS_SCREEN]: {
        screen: SelectedFiltersScreen
    }
}, {
    headerMode: 'none'
})

export {TimetableStackNavigator}