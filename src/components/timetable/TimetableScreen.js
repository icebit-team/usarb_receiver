import React, {PureComponent} from "react"
import {FlatList, Text, View, NativeModules} from "react-native"
import {Button, Container, Fab, Header, Icon, Left, Right, Title} from 'native-base'
import {BASE_COLOR, GREEN_COLOR, ORANGE_COLOR, WHITE_COLOR, WHITE_RED_COLOR, WHITE_VIOLET_COLOR, WIDTH} from "../../constants/Global"
import BaseDayTable from "./BaseDayTable"
import {getHorizontalPageNumber} from "../../utils/AppUtils"
import {connect} from "react-redux"
import {TIMETABLE_CLASSROOMS_FILTER_SCREEN, TIMETABLE_GROUPS_FILTER_SCREEN, TIMETABLE_SELECTED_FILTERS_SCREEN, TIMETABLE_TEACHERS_FILTER_SCREEN} from "../../constants/Routes"
import {addNotification, loadTimetableData} from "../../actions"
import {LoadContainer, OfflineContainer} from "../shared"
import {destroyListeners, initConfig} from "../../utils/FirebaseUtils"
import {formatFullDate} from "../../utils/DateUtils"
import I18n from "react-native-i18n"

class TimetableComponent extends PureComponent {
    state = {
        active: false
    }

    componentDidMount = async () => {
        await initConfig(this)

        //check notifications when user click on group of notifications, or app is opened without click on notification
        if (NativeModules.NotificationManagerModule) {
            NativeModules.NotificationManagerModule.checkNotifications()
        }
    }

    componentWillUnmount() {
        destroyListeners()
    }

    componentDidUpdate = () => {
        if (this.refs.flatList) {
            this.refs.flatList.scrollToIndex({animated: false, index: 1})
        }
    }

    render() {
        let {data, filter, date, load, hasInternet} = this.props,
            {navigate} = this.props.navigation

        return (
            <Container>
                <Header>
                    <Left>
                        <Title>{I18n.t('menu.timetable')}</Title>
                    </Left>
                    <Right>
                        <View>
                            <Text style={{color: WHITE_COLOR, textAlign: 'right'}} numberOfLines={1}>
                                {formatFullDate(date)}
                            </Text>
                            <Text style={{color: WHITE_COLOR, textAlign: 'right'}} numberOfLines={1}>
                                {filter ? filter.data.name : 'N/A'}
                            </Text>
                        </View>
                    </Right>
                </Header>

                {!hasInternet && <OfflineContainer/>}

                {load && <LoadContainer/>}

                {!load &&
                <FlatList horizontal={true} pagingEnabled={true} scrollEnabled={true}
                          showsHorizontalScrollIndicator={false} ref="flatList"
                          getItemLayout={(data, index) => ({length: 3, offset: WIDTH * index, index})}
                          initialScrollIndex={1} data={[{}, data, {}]}
                          keyExtractor={(item, index) => index.toString()}
                          renderItem={({item, index}) => index === 1 ? <BaseDayTable data={item}/> : <LoadContainer/>}
                          onMomentumScrollEnd={this._onMomentumScrollEnd}/>
                }

                <Fab
                    active={this.state.active} direction="up" style={{backgroundColor: BASE_COLOR}}
                    position="bottomRight"
                    onPress={() => this.setState({active: !this.state.active})}>
                    <Icon type="FontAwesome" name="filter"/>
                    <Button style={{backgroundColor: ORANGE_COLOR}} onPress={() => navigate(TIMETABLE_SELECTED_FILTERS_SCREEN)}>
                        <Icon type="FontAwesome" name="star"/>
                    </Button>
                    <Button style={{backgroundColor: GREEN_COLOR}} onPress={() => navigate(TIMETABLE_GROUPS_FILTER_SCREEN)}>
                        <Icon type="FontAwesome5" name="users"/>
                    </Button>
                    <Button style={{backgroundColor: WHITE_VIOLET_COLOR}} onPress={() => navigate(TIMETABLE_TEACHERS_FILTER_SCREEN)}>
                        <Icon type="FontAwesome5" name="graduation-cap"/>
                    </Button>
                    <Button style={{backgroundColor: WHITE_RED_COLOR}} onPress={() => navigate(TIMETABLE_CLASSROOMS_FILTER_SCREEN)}>
                        <Icon type="FontAwesome" name="sign-in"/>
                    </Button>
                </Fab>
            </Container>
        )
    }

    _onMomentumScrollEnd = (event) => {
        let pageIdx = getHorizontalPageNumber(event),
            {filter, date} = this.props

        if (pageIdx !== 1) {
            let increment = pageIdx === 0 ? -1 : 1
            this.props.loadTimetableData(filter, date, increment)
        }
    }
}

const mapStateToProps = state => {
    return {
        data: state.timetableData.data,
        date: state.timetableData.date,
        load: state.timetableData.load,
        hasInternet: state.timetableData.hasInternet,
        filter: state.timetableData.filter
    }
}

const TimetableScreen = connect(mapStateToProps, {loadTimetableData, addNotification})(TimetableComponent)

export {TimetableScreen}