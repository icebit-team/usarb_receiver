import {createAppContainer, createBottomTabNavigator, createSwitchNavigator} from "react-navigation"
import {BOTTOM_TAB_NAVIGATOR, CALENDAR_STACK_NAVIGATOR, CONTACTS_SCREEN, CRITICAL_ERROR_SCREEN, LOAD_APP_SCREEN, NOTIFICATIONS_SCREEN, SETTINGS_STACK_NAVIGATOR, TIMETABLE_STACK_NAVIGATOR} from "./constants/Routes"
import React from "react"
import {CriticalErrorScreen, LoadAppScreen, NotificationScreen} from "./components"
import {CustomIcon} from "./components/shared"
import {TimetableStackNavigator} from "./components/timetable"
import {SettingsStackNavigator} from "./components/settings"
import {CalendarStackNavigator} from "./components/calendar"
import {ContactsScreen} from "./components/contacts/ContactsScreen"
import I18n from "react-native-i18n"

const BottomTabNavigator = createBottomTabNavigator({
    [TIMETABLE_STACK_NAVIGATOR]: {
        screen: TimetableStackNavigator,
        navigationOptions: {
            tabBarLabel: I18n.t('menu.timetable'),
            tabBarIcon: ({tintColor}) => (
                <CustomIcon name="calendar-times-o" color={tintColor}/>
            )
        }
    },
    [CALENDAR_STACK_NAVIGATOR]: {
        screen: CalendarStackNavigator,
        navigationOptions: {
            tabBarLabel: I18n.t('menu.calendar'),
            tabBarIcon: ({tintColor}) => (
                <CustomIcon name="calendar" color={tintColor}/>
            )
        }
    },
    [NOTIFICATIONS_SCREEN]: {
        screen: NotificationScreen,
        navigationOptions: {
            tabBarLabel: I18n.t('menu.notifications'),
            tabBarIcon: ({tintColor}) => (
                <CustomIcon name="envelope" color={tintColor}/>
            )
        }
    },
    [CONTACTS_SCREEN]: {
        screen: ContactsScreen,
        navigationOptions: {
            tabBarLabel: I18n.t('menu.contacts'),
            tabBarIcon: ({tintColor}) => (
                <CustomIcon name="address-card-o" color={tintColor}/>
            )
        }
    },
    [SETTINGS_STACK_NAVIGATOR]: {
        screen: SettingsStackNavigator,
        navigationOptions: {
            tabBarLabel: I18n.t('menu.settings'),
            tabBarIcon: ({tintColor}) => (
                <CustomIcon name="cogs" color={tintColor}/>
            )
        }
    }
})

const AppSwitchNavigator2 = createSwitchNavigator({
    [LOAD_APP_SCREEN]: LoadAppScreen,
    [BOTTOM_TAB_NAVIGATOR]: BottomTabNavigator,
    [CRITICAL_ERROR_SCREEN]: CriticalErrorScreen
})

const AppSwitchNavigator = createAppContainer(AppSwitchNavigator2)

export {AppSwitchNavigator}