const Realm = require('realm')

let openedRealmInstance = undefined
export const realmInstance = async () => {
    if (!openedRealmInstance) {
        openedRealmInstance = await Realm.open({
            schema: [
                ClassroomEntity,
                GroupEntity,
                TeacherEntity,
                NotificationEntity,
                CalendarEntity,
                CalendarDetailsEntity,
                SemesterEntity,
                TimetableEntity,
                TimetableDetailsEntity,
                TimetableDetailItemsEntity,
                FilterEntity,
                FilterDataEntity
            ],
            schemaVersion: 18
        })
    }

    return openedRealmInstance
}

export const CLASSROOM_ENTITY = 'ClassroomEntity'
export const GROUP_ENTITY = 'GroupEntity'
export const TEACHER_ENTITY = 'TeacherEntity'
export const NOTIFICATION_ENTITY = 'NotificationEntity'
export const CALENDAR_ENTITY = 'CalendarEntity'
export const CALENDAR_DETAILS_ENTITY = 'CalendarDetailsEntity'
export const SEMESTER_ENTITY = 'SemesterEntity'
export const TIMETABLE_ENTITY = 'TimetableEntity'
export const TIMETABLE_DETAILS_ENTITY = 'TimetableDetailsEntity'
export const TIMETABLE_DETAIL_ITEMS_ENTITY = 'TimetableDetailItemsEntity'
export const FILTER_ENTITY = 'FilterEntity'
export const FILTER_DATA_ENTITY = 'FilterDataEntity'

const FilterEntity = {
    name: FILTER_ENTITY,
    primaryKey: 'filterHash',
    properties: {
        filterHash: 'string',
        data: 'FilterDataEntity',
        teacher: 'bool',
        group: 'bool',
        classroom: 'bool',
        subscribe: 'bool'
    }
}

const FilterDataEntity = {
    name: FILTER_DATA_ENTITY,
    properties: {
        id: 'int',
        databaseHash: 'string',
        database: 'string',
        topic: 'string',
        name: 'string',
    }
}

const SemesterEntity = {
    name: SEMESTER_ENTITY,
    primaryKey: 'databaseHash',
    properties: {
        id: 'int',
        database: 'string',
        databaseHash: 'string',
        date: 'string',
        semesterNumber: 'int'
    }
}

const TimetableEntity = {
    name: TIMETABLE_ENTITY,
    primaryKey: 'filterAndWeekDateHash',
    properties: {
        filterAndWeekDateHash: 'string',
        details: 'TimetableDetailsEntity[]'
    }
}

const TimetableDetailsEntity = {
    name: TIMETABLE_DETAILS_ENTITY,
    properties: {
        date: 'string',
        lessonNumber: 'int',
        items: 'TimetableDetailItemsEntity[]'
    }
}

const TimetableDetailItemsEntity = {
    name: TIMETABLE_DETAIL_ITEMS_ENTITY,
    properties: {
        lessonType: 'string',
        discipline: 'string',
        primaryInfo: 'string',
        secondaryInfo: 'string'
    }
}

const CalendarEntity = {
    name: CALENDAR_ENTITY,
    primaryKey: 'filterHash',
    properties: {
        filterHash: 'string',
        details: 'CalendarDetailsEntity[]'
    }
}

const CalendarDetailsEntity = {
    name: CALENDAR_DETAILS_ENTITY,
    properties: {
        date: 'string',
        hours: 'int'
    }
}

const ClassroomEntity = {
    name: CLASSROOM_ENTITY,
    primaryKey: 'databaseHash',
    properties: {
        id: 'int',
        database: 'string',
        databaseHash: 'string',
        name: 'string',
        topic: 'string',
        shortName: 'string',
        capacity: {type: 'int', optional: true}
    }
}

const GroupEntity = {
    name: GROUP_ENTITY,
    primaryKey: 'databaseHash',
    properties: {
        id: 'int',
        database: 'string',
        databaseHash: 'string',
        specialityId: 'int',
        name: 'string',
        topic: 'string',
        year: 'int'
    }
}

const TeacherEntity = {
    name: TEACHER_ENTITY,
    primaryKey: 'databaseHash',
    properties: {
        id: 'int',
        database: 'string',
        databaseHash: 'string',
        name: 'string',
        topic: 'string',
        nameTimetable: 'string',
        departmentId: {type: 'int', optional: true},
        personId: {type: 'int', optional: true},
        active: 'bool',
        title: {type: 'string', optional: true}
    }
}

const NotificationEntity = {
    name: NOTIFICATION_ENTITY,
    primaryKey: 'hashCode',
    properties: {
        hashCode: 'int',
        time: 'int',
        topic: 'string',
        topicInfo: 'string',
        type: 'string',
        receivedDate: 'string',
        receivedTime: 'string',
        read: 'bool',
        teacher: 'bool',
        group: 'bool',
        classroom: 'bool',
        jsonInfo: 'string'
    }
}