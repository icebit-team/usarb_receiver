export const CLASSROOMS = "/classrooms"
export const TEACHERS = "/teachers"
export const GROUPS = "/groups"
const TIMETABLE = "/timetable"
export const SEMESTERS = "/semesters/license"
export const TIMETABLE_SHORT = "/timetable/hours-per-date"

export const TIMETABLE_BY_FILTER = (filter) => {
    if (filter.group) {
        return `${TIMETABLE}/group`
    } else if (filter.teacher) {
        return `${TIMETABLE}/teacher`
    } else if (filter.classroom) {
        return `${TIMETABLE}/classroom`
    } else {
        throw new Error('Ceva n-a mers bine!')
    }
}

export const REST_CLIENT_URL = "https://rest-client.usarb.md"
// export const REST_CLIENT_URL = "http://10.0.2.2:8080"