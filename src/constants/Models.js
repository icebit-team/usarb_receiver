export class EntityModel {
    databaseHash: string
    id: number
    name: string
    database: string
    topic: string
}

export class FilterModel {
    data: EntityModel
    teacher: boolean = false
    group: boolean = false
    classroom: boolean = false
    subscribe: boolean = false
}