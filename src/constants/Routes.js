export const LOAD_APP_SCREEN = "LoadAppScreen"
export const CRITICAL_ERROR_SCREEN = "CriticalErrorScreen"
export const BOTTOM_TAB_NAVIGATOR = "BottomTabNavigator"

export const TIMETABLE_STACK_NAVIGATOR = "TimetableStackNavigator"
export const TIMETABLE_SCREEN = "TimetableScreen"
export const TIMETABLE_TEACHERS_FILTER_SCREEN = "TimetableTeachersFilterScreen"
export const TIMETABLE_GROUPS_FILTER_SCREEN = "TimetableGroupsFilterScreen"
export const TIMETABLE_CLASSROOMS_FILTER_SCREEN = "TimetableClassroomsFilterScreen"
export const TIMETABLE_SELECTED_FILTERS_SCREEN = "TimetableSelectedFiltersScreen"

export const CALENDAR_STACK_NAVIGATOR = "CalendarStackNavigator"
export const CALENDAR_SCREEN = "CalendarScreen"
export const CALENDAR_FULL_SCREEN = "CalendarFullScreen"
export const CALENDAR_TEACHERS_FILTER_SCREEN = "CalendarTeachersFilterScreen"
export const CALENDAR_GROUPS_FILTER_SCREEN = "CalendarGroupsFilterScreen"
export const CALENDAR_CLASSROOMS_FILTER_SCREEN = "CalendarClassroomsFilterScreen"
export const CALENDAR_SELECTED_FILTERS_SCREEN = "CalendarSelectedFiltersScreen"

export const NOTIFICATIONS_SCREEN = "NotificationsScreen"

export const CONTACTS_SCREEN = "ContactsScreen"

export const SETTINGS_STACK_NAVIGATOR = "SettingsStackNavigator"
export const SETTINGS_SCREEN = "SettingsScreen"
export const SET_TEACHERS_SCREEN = "SetTeachersScreen"
export const SET_GROUPS_SCREEN = "SetGroupsScreen"
export const SET_CLASSROOMS_SCREEN = "SetClassroomsScreen"