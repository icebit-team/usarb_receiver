import {Dimensions, Platform} from "react-native"

export const NOTIFICATION_CHANNEL = "USARB-Receiver-Channel"

export const DEFAULT_DATE_FORMAT = 'DD/MM/YYYY'

export const CALENDAR_WEEK_DAYS = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']

export const LESSON_TIME = {
    1: '8:00 - 9:30',
    2: '9:40 - 11:10',
    3: '11:30 - 13:00',
    4: '13:10 - 14:40',
    5: '14:50 - 16:20',
    6: '16:30 - 18:00',
    7: '18:10 - 19:40'
}

export const PAGE_TAB_SIZE = 25
export const PAGE_SMALL_TAB_SIZE = 7

export const BASE_COLOR = "#345fc8"
export const WHITE_BASE_COLOR = "#c1fffc"
export const RED_COLOR = "#c81d00"
export const WHITE_RED_COLOR = "#fe7443"
export const WHITE_RED_COLOR_2 = "#feba95"
export const ORANGE_COLOR = "#feac53"
export const WHITE_ORANGE_COLOR = "#fad164"
export const WHITE_YALOW_COLOR = "#fbf5ab"
export const GREEN_COLOR = "#23a568"
export const WHITE_GREEN_COLOR = "#75f8a1"
export const WHITE_GREEN_COLOR_2 = "#a8f8c1"
export const WHITE_COLOR = "#ffffff"
export const GREY_COLOR = "#cccccc"
export const BLACK_GREY_COLOR = "#5f5f5f"
export const WHITE_GREY_COLOR = "#eeeeee"
export const WHITE_VIOLET_COLOR = "#a27add"

export const HEADER_HEIGHT = Platform.select({
    ios: 70,
    android: 56
})

export const EMPTY_FN = () => {
}

export const RO_LANG = 'ro'
export const RU_LANG = 'ru'
export const EN_LANG = 'en'

export const WINDOW = Dimensions.get("window")
export const WIDTH = WINDOW.width
export const HEIGHT = WINDOW.height

export const ICON_SIZE = 24
export const ICON_MEDIUM_SIZE = 18
export const ICON_SMALL_SIZE = 15
export const ICON_VERY_SMALL_SIZE = 12

/* STORAGE constants >> */

export const CURRENT_FILTER = "Hftunx2ESrfCeMz"
export const SUBSCRIBED_TOPICS = "5qPqTA4UpLsZtS7"
export const SELECTED_LANG = "OGo46EAXNYJ7j0V"

/* << STORAGE constants */