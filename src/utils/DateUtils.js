import moment from "moment"
import I18n from "react-native-i18n"

export const formatFullDate = (momentDate) => {
    let date = moment(momentDate),
        dayName = I18n.t(['days', date.format('dddd').toLowerCase()]),
        monthName = I18n.t(['months', date.format('MMMM').toLowerCase()])

    return `${dayName} ${date.date()} ${monthName} ${date.year()}`
}

export const formatShortDate = (momentDate) => {
    let date = moment(momentDate),
        dayName = I18n.t(['shortDays2', date.format('dddd').toLowerCase()]),
        monthName = I18n.t(['months', date.format('MMMM').toLowerCase()])

    return `${dayName}. ${date.date()} ${monthName}`
}