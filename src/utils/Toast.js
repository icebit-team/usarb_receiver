import {Toast} from "native-base"

const DURATION = 2000

export const WARNING = (msg) => {
    Toast.show({
        text: msg,
        buttonText: "Ok",
        type: "warning",
        duration: DURATION
    })
}

export const ERROR = (msg) => {
    Toast.show({
        text: msg,
        buttonText: "Ok",
        type: "danger",
        duration: DURATION
    })
}

export const SUCCESS = (msg) => {
    Toast.show({
        text: msg,
        buttonText: "Ok",
        type: "success",
        duration: DURATION
    })
}