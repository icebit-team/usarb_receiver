import {ERROR} from "./Toast"
import axios from "axios"
import {REST_CLIENT_URL} from "../constants/Paths"

export const GET_REQUEST = async (url, params) => {
    try {
        const response = await axios({
            method: 'get',
            url: `${REST_CLIENT_URL}${url}`,
            params: params
        })

        if (response.request.status === 200) {
            return response.data
        } else {
            return await processBadResponse(url, params)
        }
    } catch (e) {
        return await processBadResponse(url, params)
    }
}

const processBadResponse = async (url, params) => {
    ERROR('Eroare de SERVER!')
    throw new Error(`Ceva n-a mers bine! URL: ${url}; PARAMS: ${JSON.stringify(params)}`)
}
