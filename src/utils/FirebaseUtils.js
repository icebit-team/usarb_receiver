import firebase from "react-native-firebase"
import {DEFAULT_DATE_FORMAT, EMPTY_FN, NOTIFICATION_CHANNEL, SUBSCRIBED_TOPICS} from "../constants/Global"
import {AsyncStorage, NativeModules, Platform} from "react-native"
import {NOTIFICATIONS_SCREEN} from "../constants/Routes"
import moment from "moment"
import I18n from "react-native-i18n";
import {SUCCESS} from "./Toast";

export const initConfig = async (scope) => {
    const channel = new firebase.notifications.Android.Channel(
        NOTIFICATION_CHANNEL, 'USARB ReceIVer', firebase.notifications.Android.Importance.Max
    ).setDescription('USARB ReceIVer Firebase Notification Channel')

    firebase.notifications().android.createChannel(channel)

    await listenNotifications(scope)
}

export const subscribeToTopic = async (topic) => {
    if (topic) {
        firebase.messaging().subscribeToTopic(topic)
        await addTopic(topic);
    }
}

export const unsubscribeFromTopic = async (topic) => {
    firebase.messaging().unsubscribeFromTopic(topic)
    await removeTopic(topic)
}

const addTopic = async (topic) => {
    const storeString = await AsyncStorage.getItem(SUBSCRIBED_TOPICS),
        store = storeString ? JSON.parse(storeString) : []

    if (!store.some(value => value === topic)) {
        store.push(topic);
        await AsyncStorage.setItem(SUBSCRIBED_TOPICS, JSON.stringify(store))
    }
}

const removeTopic = async (topic) => {
    const storeString = await AsyncStorage.getItem(SUBSCRIBED_TOPICS),
        store = storeString ? JSON.parse(storeString) : []

    if (store.some(value => value === topic)) {
        let newStore = store.filter(value => value !== topic);
        await AsyncStorage.setItem(SUBSCRIBED_TOPICS, JSON.stringify(newStore))
    }
}

let getInitialNotificationListener = EMPTY_FN
let onNotificationListener = EMPTY_FN
let onNotificationOpenedListener = EMPTY_FN

export const hasAccess = async () => {
    try {
        await firebase.messaging().requestPermission()
        const fcmToken = await firebase.messaging().getToken()

        if (!fcmToken) {
            console.log('FCM Token not available')
            return false
        }

        console.log('FCM Token: ', fcmToken)

        return await firebase.messaging().hasPermission()
    } catch (e) {
        return false
    }
}

const afterReceiveNotification = (notificationOpen) => {
    if (NativeModules.NotificationManagerModule) {
        //process remain notifications
        NativeModules.NotificationManagerModule.checkNotifications()
    } else {
        if(Platform.OS === 'android' && typeof notificationOpen.notification.android.tag === 'string'){
            firebase.notifications().android.removeDeliveredNotificationsByTag(notificationOpen.notification.android.tag)
        } else {
            firebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId)
        }
    }
}

const listenNotifications = async (scope) => {
    try {
        const enabled = await hasAccess()
        console.log('FCM messaging has permission: ' + enabled)

        if (enabled) {
            const oldTopicsJson = await AsyncStorage.getItem(SUBSCRIBED_TOPICS),
                oldTopics = oldTopicsJson ? JSON.parse(oldTopicsJson) : []

            oldTopics.forEach(value => firebase.messaging().subscribeToTopic(value))

            //Foreground
            getInitialNotificationListener = firebase.notifications().getInitialNotification().then(notificationOpen => {
                if (notificationOpen) {
                    //save opened notification
                    saveNotification(notificationOpen.notification, scope)
                    afterReceiveNotification(notificationOpen)

                    scope.props.navigation.navigate(NOTIFICATIONS_SCREEN)
                }
            })

            //Opened
            onNotificationListener = firebase.notifications().onNotification((notification) => {
                saveNotification(notification, scope)

                if (!NativeModules.NotificationManagerModule) {
                    SUCCESS(I18n.t('msg.receiveNotifications'))
                    firebase.notifications().removeAllDeliveredNotifications()
                } else {
                    notification.android.setChannelId(NOTIFICATION_CHANNEL).android.setSmallIcon('usarb_ic_launcher').setSound("default")
                    firebase.notifications().displayNotification(notification)
                }
            })

            //Background
            onNotificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
                if (notificationOpen) {
                    //process opened notification
                    saveNotification(notificationOpen.notification, scope)
                    afterReceiveNotification(notificationOpen)

                    scope.props.navigation.navigate(NOTIFICATIONS_SCREEN)
                }
            })
        }
    } catch (e) {
        console.log('Error initializing FCM', e);
    }
}

const saveNotification = (notification, scope) => {
    let dateTime = moment()

    scope.props.addNotification({
        hashCode: parseInt(notification.data.hashCode),
        time: dateTime.toDate().getTime(),
        receivedDate: dateTime.format(DEFAULT_DATE_FORMAT),
        receivedTime: dateTime.format('H:mm:ss'),
        read: false,
        topic: notification.data.topic,
        topicInfo: notification.data.topicInfo,
        type: notification.data.type,
        teacher: notification.data.teacher === 'true',
        group: notification.data.group === 'true',
        classroom: notification.data.classroom === 'true',
        jsonInfo: notification.data.jsonInfo
    })
}

export const destroyListeners = () => {
    getInitialNotificationListener()
    onNotificationListener()
    onNotificationOpenedListener()
}