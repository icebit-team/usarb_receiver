import {FilterModel} from "../constants/Models"

export const getHorizontalPageNumber = (event) => {
    let {contentOffset, layoutMeasurement} = event.nativeEvent
    return parseInt((contentOffset.x + layoutMeasurement.width) / layoutMeasurement.width) - 1
}

export const SLEEP = (seconds) => {
    const e = new Date().getTime() + (seconds * 1000)
    while (new Date().getTime() <= e) {
    }
}

export const ASYNC_CALL = (callback) => {
    setTimeout(() => callback(), 0)
}

export const REPLACE_RO_CHARS = (value: string) => value
    .replace(/[\u0103\u00e2]/g, 'a') //ăâ
    .replace(/[\u0102\u00c2]/g, 'A') //ĂÂ
    .replace(/\u00ee/g, 'i') //î
    .replace(/\u00ce/g, 'I') //Î
    .replace(/[\u0219\u015f]/g, 's') //șş
    .replace(/[\u0218\u015e]/g, 'S') //ȘŞ
    .replace(/[\u021b\u0163]/g, 't') //țţ
    .replace(/[\u021a\u0162]/g, 'T') //ȚŢ

export const isSameFilter = (f1: FilterModel, f2: FilterModel) => {
    let isSameType = Object.keys(f1).find(key => f1[key] === true) === Object.keys(f2).find(key => f2[key] === true)
    return isSameType && f1.data.name === f2.data.name
}

export const buildFilterHash = (filter: FilterModel) => {
    let filterHash = filter.data.name

    if (filter.group) {
        filterHash = filterHash + 'GROUP'
    } else if (filter.teacher) {
        filterHash = filterHash + 'TEACHER'
    } else if (filter.classroom) {
        filterHash = filterHash + 'CLASSROOM'
    }

    return filterHash
}