import {GET_TIMETABLE_DATA_REQUEST, GET_TIMETABLE_DATA_SUCCESS} from "../constants/Types"
import moment from 'moment'
import {AsyncStorage, NetInfo} from "react-native"
import {CURRENT_FILTER, DEFAULT_DATE_FORMAT} from "../constants/Global"
import {GET_REQUEST} from "../utils/Request"
import {TIMETABLE_BY_FILTER} from "../constants/Paths"
import {buildFilterHash, isSameFilter} from "../utils/AppUtils"
import {TIMETABLE_ENTITY, realmInstance} from "../constants/Realm"

let prevFilter = undefined
let prevStartDate = undefined
let prevEndDate = undefined
let data = undefined

const loadTimetableData = (filter = null, date = null, increment = 0) => async (dispatch) => {
    let currentDate = increment === 0 && !date ? moment() : moment(date).add(increment, 'day'),
        stringWeekDate = currentDate.format(DEFAULT_DATE_FORMAT),
        hasInternet = await NetInfo.isConnected.fetch()

    if (!filter) {
        filter = await JSON.parse(await AsyncStorage.getItem(CURRENT_FILTER))
    }

    if (!filter) {
        dispatch({
            type: GET_TIMETABLE_DATA_SUCCESS,
            payload: {
                data: [],
                date: currentDate,
                hasInternet: hasInternet
            }
        })

    } else {
        if (data && currentDate.isBetween(prevStartDate, prevEndDate, null, '[]') && isSameFilter(prevFilter, filter)) {
            dispatch({
                type: GET_TIMETABLE_DATA_SUCCESS,
                payload: {
                    data: data.filter(item => item.date === stringWeekDate),
                    date: currentDate,
                    filter: filter,
                    hasInternet: hasInternet
                }
            })
        } else {
            let startDate = moment(currentDate).startOf('isoWeek'),
                endDate = moment(startDate).add(5, 'day'),
                realm = await realmInstance(),
                filterHash = buildFilterHash(filter) + stringWeekDate

            await AsyncStorage.setItem(CURRENT_FILTER, JSON.stringify(filter))

            dispatch({
                type: GET_TIMETABLE_DATA_REQUEST,
                payload: {
                    date: currentDate,
                    filter: filter
                }
            })

            prevFilter = filter
            prevStartDate = startDate
            prevEndDate = endDate

            if (hasInternet) {
                data = await GET_REQUEST(TIMETABLE_BY_FILTER(filter), {
                    weekDate: stringWeekDate,
                    groupName: filter.group ? filter.data.name : undefined,
                    teacherName: filter.teacher ? filter.data.name : undefined,
                    classroomName: filter.classroom ? filter.data.name : undefined
                })

                if (realm.objectForPrimaryKey(TIMETABLE_ENTITY, filterHash)) {
                    realm.write(() => realm.delete(realm.objectForPrimaryKey(TIMETABLE_ENTITY, filterHash)))
                }

                realm.write(() => realm.create(TIMETABLE_ENTITY, {
                    filterAndWeekDateHash: filterHash,
                    details: data
                }))
            } else {
                let calendarData = realm.objectForPrimaryKey(TIMETABLE_ENTITY, filterHash)
                data = calendarData ? calendarData.details : []
            }

            dispatch({
                type: GET_TIMETABLE_DATA_SUCCESS,
                payload: {
                    data: data.filter(item => item.date === stringWeekDate),
                    date: currentDate,
                    filter: filter,
                    hasInternet: hasInternet
                }
            })
        }
    }
}

export {loadTimetableData}