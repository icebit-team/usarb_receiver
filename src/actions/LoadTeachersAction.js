import {GET_TEACHERS_BY_FILTER, GET_TEACHERS_DATA_REFRESH, GET_TEACHERS_DATA_SUCCESS, GET_TEACHERS_NEXT} from "../constants/Types"
import {GET_REQUEST} from "../utils/Request"
import {TEACHERS} from "../constants/Paths"
import {PAGE_TAB_SIZE} from "../constants/Global"
import {realmInstance, TEACHER_ENTITY} from "../constants/Realm"
import {REPLACE_RO_CHARS} from "../utils/AppUtils"
import {NetInfo} from "react-native"

let prevPartOfName = undefined

const loadTeachers = () => async (dispatch) => {
    let realm = await realmInstance(),
        hasInternet = await NetInfo.isConnected.fetch()

    dispatch({
        type: GET_TEACHERS_DATA_REFRESH
    })

    if (hasInternet) {
        let remoteData = await GET_REQUEST(TEACHERS, {
            active: true
        })

        realm.write(() => realm.delete(realm.objects(TEACHER_ENTITY)))
        realm.write(() => remoteData.forEach(group => realm.create(TEACHER_ENTITY, group)))
    }

    let data = realm.objects(TEACHER_ENTITY)

    let filtered = data.filter(item => !prevPartOfName || REPLACE_RO_CHARS(item.name).toLowerCase().includes(REPLACE_RO_CHARS(prevPartOfName).toLowerCase()))
    let result = filtered.slice(0, PAGE_TAB_SIZE)

    dispatch({
        type: GET_TEACHERS_DATA_SUCCESS,
        payload: {
            data: result,
            last: data.length === result.length,
            hasInternet: hasInternet,
            filter: prevPartOfName
        }
    })
}

const loadTeachersByFilter = (partOfName) => async (dispatch) => {
    let realm = await realmInstance(),
        data = realm.objects(TEACHER_ENTITY)

    prevPartOfName = partOfName.trim()
    let filtered = prevPartOfName.length > 0 ? data.filter(item => REPLACE_RO_CHARS(item.name).toLowerCase().includes(REPLACE_RO_CHARS(prevPartOfName).toLowerCase())) : data
    let result = filtered.slice(0, PAGE_TAB_SIZE)

    dispatch({
        type: GET_TEACHERS_BY_FILTER,
        payload: {
            data: result.slice(0, PAGE_TAB_SIZE),
            filter: prevPartOfName,
            last: filtered.length === result.length
        }
    })
}

const loadNextTeachers = (viewSize) => async (dispatch) => {
    let realm = await realmInstance(),
        data = realm.objects(TEACHER_ENTITY)

    let filtered = data.filter(item => !prevPartOfName || REPLACE_RO_CHARS(item.name).toLowerCase().includes(REPLACE_RO_CHARS(prevPartOfName).toLowerCase()))
    let result = filtered.slice(0, viewSize + PAGE_TAB_SIZE)

    dispatch({
        type: GET_TEACHERS_NEXT,
        payload: {
            data: result,
            last: filtered.length === result.length
        }
    })
}

export {loadTeachers, loadTeachersByFilter, loadNextTeachers}