import {GET_CLASSROOMS_BY_FILTER, GET_CLASSROOMS_DATA_REFRESH, GET_CLASSROOMS_DATA_SUCCESS, GET_CLASSROOMS_NEXT} from "../constants/Types"
import {GET_REQUEST} from "../utils/Request"
import {CLASSROOMS} from "../constants/Paths"
import {PAGE_TAB_SIZE} from "../constants/Global"
import {CLASSROOM_ENTITY, realmInstance} from "../constants/Realm"
import {REPLACE_RO_CHARS} from "../utils/AppUtils"
import {NetInfo} from "react-native"

let prevPartOfName = undefined

const loadClassrooms = () => async (dispatch) => {
    let realm = await realmInstance(),
        hasInternet = await NetInfo.isConnected.fetch()

    dispatch({
        type: GET_CLASSROOMS_DATA_REFRESH
    })

    if (hasInternet) {
        let remoteData = await GET_REQUEST(CLASSROOMS)

        realm.write(() => realm.delete(realm.objects(CLASSROOM_ENTITY)))
        realm.write(() => remoteData.forEach(group => realm.create(CLASSROOM_ENTITY, group)))
    }

    let data = realm.objects(CLASSROOM_ENTITY)

    let filtered = data.filter(item => !prevPartOfName || REPLACE_RO_CHARS(item.name).toLowerCase().includes(REPLACE_RO_CHARS(prevPartOfName).toLowerCase()))
    let result = filtered.slice(0, PAGE_TAB_SIZE)

    dispatch({
        type: GET_CLASSROOMS_DATA_SUCCESS,
        payload: {
            data: result,
            last: data.length === result.length,
            hasInternet: hasInternet,
            filter: prevPartOfName
        }
    })
}

const loadClassroomsByFilter = (partOfName) => async (dispatch) => {
    let realm = await realmInstance(),
        data = realm.objects(CLASSROOM_ENTITY)

    prevPartOfName = partOfName.trim()
    let filtered = prevPartOfName.length > 0 ? data.filter(item => REPLACE_RO_CHARS(item.name).toLowerCase().includes(REPLACE_RO_CHARS(prevPartOfName).toLowerCase())) : data
    let result = filtered.slice(0, PAGE_TAB_SIZE)

    dispatch({
        type: GET_CLASSROOMS_BY_FILTER,
        payload: {
            data: result,
            filter: prevPartOfName,
            last: filtered.length === result.length
        }
    })
}

const loadNextClassrooms = (viewSize) => async (dispatch) => {
    let realm = await realmInstance(),
        data = realm.objects(CLASSROOM_ENTITY)

    let filtered = data.filter(item => !prevPartOfName || REPLACE_RO_CHARS(item.name).toLowerCase().includes(REPLACE_RO_CHARS(prevPartOfName).toLowerCase()))
    let result = filtered.slice(0, viewSize + PAGE_TAB_SIZE)

    dispatch({
        type: GET_CLASSROOMS_NEXT,
        payload: {
            data: result,
            last: filtered.length === result.length
        }
    })
}

export {loadClassrooms, loadClassroomsByFilter, loadNextClassrooms}