import {CHANGE_SELECTED_FILTERS} from "../constants/Types"
import {FilterModel} from "../constants/Models"
import {FILTER_ENTITY, realmInstance} from "../constants/Realm"
import {buildFilterHash} from "../utils/AppUtils"
import {subscribeToTopic, unsubscribeFromTopic} from "../utils/FirebaseUtils";

const selectFilter = (filter: FilterModel) => async (dispatch) => {
    let realm = await realmInstance(),
        filters = realm.objects(FILTER_ENTITY)

    realm.write(() => {
        let isFirst = filters.length === 0;

        realm.create(FILTER_ENTITY, {
            filterHash: buildFilterHash(filter),
            data: {
                id: filter.data.id,
                databaseHash: filter.data.databaseHash,
                database: filter.data.database,
                name: filter.data.name,
                topic: filter.data.topic
            },
            teacher: !!filter.teacher,
            group: !!filter.group,
            classroom: !!filter.classroom,
            subscribe: isFirst
        })

        if (isFirst) {
            subscribeToTopic(filter.data.topic)
        }
    })

    dispatch({
        type: CHANGE_SELECTED_FILTERS,
        payload: realm.objects(FILTER_ENTITY)
    })
}

const subscribeToFilter = (filter: FilterModel) => async (dispatch) => {
    let realm = await realmInstance()

    realm.write(() => {
        let dbFilter = realm.objectForPrimaryKey(FILTER_ENTITY, buildFilterHash(filter))
        dbFilter.subscribe = !filter.subscribe

        //todo remove after 2-3 months
        realm.delete(realm.objects(FILTER_ENTITY)
            .filter(item => !item.data.topic))

        if (dbFilter.subscribe) {
            subscribeToTopic(filter.data.topic)
        } else {
            unsubscribeFromTopic(filter.data.topic)
        }
    })

    dispatch({
        type: CHANGE_SELECTED_FILTERS,
        payload: realm.objects(FILTER_ENTITY)
    })
}

const configFilter = () => async (dispatch) => {
    let realm = await realmInstance()

    dispatch({
        type: CHANGE_SELECTED_FILTERS,
        payload: realm.objects(FILTER_ENTITY)
    })
}

const unselectFilter = (filter: FilterModel) => async (dispatch) => {
    let realm = await realmInstance()
    realm.write(() => {
        realm.delete(realm.objectForPrimaryKey(FILTER_ENTITY, buildFilterHash(filter)))
        unsubscribeFromTopic(filter.data.topic)
    })

    dispatch({
        type: CHANGE_SELECTED_FILTERS,
        payload: realm.objects(FILTER_ENTITY)
    })
}

const clearFilterStorage = () => async (dispatch) => {
    let realm = await realmInstance()
    realm.write(() => {
        realm.objects(FILTER_ENTITY).forEach(filter => unsubscribeFromTopic(filter.data.topic))
        realm.delete(realm.objects(FILTER_ENTITY))
    })

    dispatch({
        type: CHANGE_SELECTED_FILTERS,
        payload: realm.objects(FILTER_ENTITY)
    })
}

export {selectFilter, configFilter, unselectFilter, clearFilterStorage, subscribeToFilter}