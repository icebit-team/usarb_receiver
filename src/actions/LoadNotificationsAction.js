import {GET_NOTIFICATIONS_DATA_SUCCESS} from "../constants/Types"
import {NOTIFICATION_ENTITY, realmInstance} from "../constants/Realm"
import {PAGE_SMALL_TAB_SIZE} from "../constants/Global";

const load = async (dispatch, viewSize = 0) => {
    let realm = await realmInstance(),
        data = realm.objects(NOTIFICATION_ENTITY).sorted('time', true)
    
    let result = data.slice(0, viewSize + PAGE_SMALL_TAB_SIZE)

    dispatch({
        type: GET_NOTIFICATIONS_DATA_SUCCESS,
        payload: {
            data: result,
            unReadCount: data.filter(not => !not.read).length,
            total: data.length,
            last: data.length === result.length
        }
    })
}

const loadNotifications = () => async (dispatch) => await load(dispatch)
const loadNextNotifications = (viewSize) => async (dispatch) => await load(dispatch, viewSize)

const clearNotifications = () => async (dispatch) => {
    let realm = await realmInstance()

    realm.write(() => {
        realm.delete(realm.objects(NOTIFICATION_ENTITY))
    })

    await load(dispatch)
}

const addNotification = (item) => async (dispatch) => {
    let realm = await realmInstance()

    realm.write(() => {
        let notification = realm.objectForPrimaryKey(NOTIFICATION_ENTITY, item.hashCode)

        if (!notification) {
            realm.create(NOTIFICATION_ENTITY, item)
        }
    })

    await load(dispatch)
}

const readNotification = (item, viewSize) => async (dispatch) => {
    let realm = await realmInstance()

    realm.write(() => {
        let notification = realm.objectForPrimaryKey(NOTIFICATION_ENTITY, item.hashCode)
        notification.read = true
    })

    await load(dispatch, viewSize)
}

export {loadNotifications, clearNotifications, readNotification, addNotification, loadNextNotifications}