import {GET_CALENDAR_DATA_REQUEST, GET_CALENDAR_DATA_SUCCESS} from "../constants/Types"
import {AsyncStorage, NetInfo} from "react-native"
import {CURRENT_FILTER} from "../constants/Global"
import {GET_REQUEST} from "../utils/Request"
import {TIMETABLE_SHORT} from "../constants/Paths"
import {buildFilterHash, isSameFilter} from "../utils/AppUtils"
import {CALENDAR_ENTITY, realmInstance} from "../constants/Realm"
import {FilterModel} from "../constants/Models"

let prevFilter = undefined

const loadCalendarData = (filter: FilterModel = null) => async (dispatch) => {
    let hasInternet = await NetInfo.isConnected.fetch()

    if (!filter) {
        filter = await JSON.parse(await AsyncStorage.getItem(CURRENT_FILTER))
    }

    if (!filter) {
        dispatch({
            type: GET_CALENDAR_DATA_SUCCESS,
            payload: {
                data: [],
                hasInternet: hasInternet
            }
        })

    } else if (!prevFilter || !isSameFilter(filter, prevFilter)) {
        let filterHash = buildFilterHash(filter)
        await AsyncStorage.setItem(CURRENT_FILTER, JSON.stringify(filter))

        dispatch({
            type: GET_CALENDAR_DATA_REQUEST,
            payload: {
                filter: filter
            }
        })

        let data,
            realm = await realmInstance()

        if (hasInternet) {
            data = await GET_REQUEST(TIMETABLE_SHORT, {
                groupName: filter.group ? filter.data.name : undefined,
                teacherName: filter.teacher ? filter.data.name : undefined,
                classroomName: filter.classroom ? filter.data.name : undefined
            })

            if (realm.objectForPrimaryKey(CALENDAR_ENTITY, filterHash)) {
                realm.write(() => realm.delete(realm.objectForPrimaryKey(CALENDAR_ENTITY, filterHash)))
            }

            realm.write(() => realm.create(CALENDAR_ENTITY, {
                filterHash: filterHash,
                details: data
            }))
        } else {
            let calendarData = realm.objectForPrimaryKey(CALENDAR_ENTITY, filterHash)
            data = calendarData ? calendarData.details : []
        }

        prevFilter = filter

        dispatch({
            type: GET_CALENDAR_DATA_SUCCESS,
            payload: {
                data: data,
                filter: filter,
                hasInternet: hasInternet
            }
        })
    }
}

export {loadCalendarData}