import {GET_SEMESTERS_DATA_SUCCESS} from "../constants/Types"
import {GET_REQUEST} from "../utils/Request"
import {SEMESTERS} from "../constants/Paths"
import {NetInfo} from "react-native"
import {realmInstance, SEMESTER_ENTITY} from "../constants/Realm"

const loadSemesters = () => async (dispatch) => {
    let data,
        hasInternet = await NetInfo.isConnected.fetch(),
        realm = await realmInstance()

    if (hasInternet) {
        data = await GET_REQUEST(SEMESTERS)

        realm.write(() => realm.delete(realm.objects(SEMESTER_ENTITY)))
        realm.write(() => data.forEach(item => realm.create(SEMESTER_ENTITY, item)))
    } else {
        data = realm.objects(SEMESTER_ENTITY)
    }

    dispatch({
        type: GET_SEMESTERS_DATA_SUCCESS,
        payload: data
    })
}

export {loadSemesters}