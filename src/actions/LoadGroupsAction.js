import {GET_GROUPS_BY_FILTER, GET_GROUPS_DATA_REFRESH, GET_GROUPS_DATA_SUCCESS, GET_GROUPS_NEXT} from "../constants/Types"
import {GET_REQUEST} from "../utils/Request"
import {GROUPS} from "../constants/Paths"
import {PAGE_TAB_SIZE} from "../constants/Global"
import {GROUP_ENTITY, realmInstance} from "../constants/Realm"
import {NetInfo} from "react-native"

let prevPartOfName = undefined

const loadGroups = () => async (dispatch) => {
    let realm = await realmInstance(),
        hasInternet = await NetInfo.isConnected.fetch()

    dispatch({
        type: GET_GROUPS_DATA_REFRESH
    })

    if (hasInternet) {
        let remoteData = await GET_REQUEST(GROUPS)

        realm.write(() => realm.delete(realm.objects(GROUP_ENTITY)))
        realm.write(() => remoteData.forEach(group => realm.create(GROUP_ENTITY, group)))
    }

    let data = realm.objects(GROUP_ENTITY)

    let filtered = data.filter(item => !prevPartOfName || item.name.toLowerCase().includes(prevPartOfName.toLowerCase()))
    let result = filtered.slice(0, PAGE_TAB_SIZE)

    dispatch({
        type: GET_GROUPS_DATA_SUCCESS,
        payload: {
            data: result,
            last: data.length === result.length,
            hasInternet: hasInternet,
            filter: prevPartOfName
        }
    })
}

const loadGroupsByFilter = (partOfName) => async (dispatch) => {
    let realm = await realmInstance(),
        data = realm.objects(GROUP_ENTITY)

    prevPartOfName = partOfName.trim()
    let filtered = prevPartOfName.length > 0 ? data.filter(item => item.name.toLowerCase().includes(prevPartOfName.toLowerCase())) : data
    let result = filtered.slice(0, PAGE_TAB_SIZE)

    dispatch({
        type: GET_GROUPS_BY_FILTER,
        payload: {
            data: result,
            filter: prevPartOfName,
            last: filtered.length === result.length
        }
    })
}

const loadNextGroups = (viewSize) => async (dispatch) => {
    let realm = await realmInstance(),
        data = realm.objects(GROUP_ENTITY)

    let filtered = data.filter(item => !prevPartOfName || item.name.toLowerCase().includes(prevPartOfName.toLowerCase()))
    let result = filtered.slice(0, viewSize + PAGE_TAB_SIZE)

    dispatch({
        type: GET_GROUPS_NEXT,
        payload: {
            data: result,
            last: filtered.length === result.length
        }
    })
}

export {loadGroups, loadGroupsByFilter, loadNextGroups}