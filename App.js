import React, {PureComponent} from 'react';
import {Provider} from 'react-redux'
import {applyMiddleware, createStore} from 'redux'
import reducers from './src/reducers'
import ReduxThunk from 'redux-thunk'
import {Root} from "native-base";
import {AppSwitchNavigator} from "./src/index";
import I18n from "react-native-i18n";
import {SELECTED_LANG} from "./src/constants/Global";
import {AsyncStorage} from "react-native";
import {LoadContainer} from "./src/components/shared";
import ro from "./locales/ro";
import en from "./locales/en";
import ru from "./locales/ru";

I18n.fallbacks = true;
I18n.translations = {ro, en, ru};

const store = createStore(reducers, applyMiddleware(ReduxThunk));

export default class App extends PureComponent {
    state = {
        init: false
    }

    componentWillMount = () => {
        AsyncStorage.getItem(SELECTED_LANG).then((val) => {
            I18n.locale = val
            this.setState({init: true})
        })
    }

    render() {
        if (this.state.init) {
            return (
                <Root>
                    <Provider store={store}>
                        <AppSwitchNavigator/>
                    </Provider>
                </Root>
            )
        } else {
            return (<LoadContainer/>)
        }
    }
}