#CMD
```
$ npm install -g react-native-cli
```
##### Run Develop
```
$ react-native run-android
```
##### Build Production
```
$ .\gradlew assembleRelease
$ .\gradlew bundleRelease

```

#Plugins
- ESLint(something like Sonar)
- React snippets 
- ReactNativeTools 

#Links
- [JSX to React](https://babeljs.io/)
- [Style Sheet](https://github.com/vhpoet/react-native-styling-cheat-sheet)
- [css-tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [LifeCycle](http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/)
- [SIGN apk](https://facebook.github.io/react-native/docs/signed-apk-android)

#Icons
- [React Native](https://oblador.github.io/react-native-vector-icons/)
```
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
<MaterialIcons name={name} size={ICON_SIZE} style={{color: WHITE_COLOR}} onPress={onPress}/>
```
```
import FontAwesome from "react-native-vector-icons/FontAwesome";
<FontAwesome ... />
```

#Hot Key (emulator)
- ``Ctrl + M `` - show menu (debug url ``http://localhost:8081/debugger-ui``)

#SEND Notification
#####POST REQUEST TO 
`https://fcm.googleapis.com/fcm/send`

#####Firebase SERVER KEY
`AAAANrKWgf4:APA91bHA1A7aODFdv6tnP9aWhB4eT5dG3Inzx9vh3P1FnStjfzFzddpKCgtfQydDYMptY0uPl7ZKOewFLCOQNMyNc0H-9vvikvVkfI9pNpHf9HdHDzbNBBD4VpCQ7BKTYdrBHqu7r2AT`

#####HEADERS
```json
[{
    "key":"Authorization",
    "value":"key=SERVER KEY"
},{
    "key":"Content-Type",
    "value":"application/json"
}]
```

#####BODY
```json
{
	"condition": "'TOPIC_TEST1' in topics || 'TOPIC_TEST2' in topics",
	"priority": "high",
	"notification": {
		"title": "TEST title", 
		"body": "TEST body",
		"sound": "default"
	},
	"data": {
		"customField1": "additional string value 1",
		"customField2": "additional string value 2"
	}
}
```